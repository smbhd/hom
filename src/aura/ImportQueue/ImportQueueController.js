/**
 * Created by AlexSanborn on 3/21/2017.
 */
({
    doInit : function(component, event, helper){
        var action = component.get("c.next");
        var actionBranchOptions = component.get("c.getAllBranches");

        helper.showSpinner(component, event);
        action.setCallback(this, function(response){
            if (response.getState() === "SUCCESS") {
                component.set("v.purchaseOrders", response.getReturnValue());

            } else {
                var errors = component.find("errors");
                errors.set('v.class', '');
                errors.get('v.body')[0].set("v.value", response.getError()[0].message);
            }
            helper.hideSpinner(component, event);
        });
        $A.enqueueAction(action);

         actionBranchOptions.setCallback(this, function(response){
             if (response.getState() === "SUCCESS") {
                 component.set("v.branchList", response.getReturnValue());
             } else {
                 var errors = component.find("errors");
                 errors.set('v.class', '');
                 errors.get('v.body')[0].set("v.value", response.getError()[0].message);
             }
         });
         $A.enqueueAction(actionBranchOptions);
    },
    //

    nextPage : function(component, event, helper){
        var currentIndex = component.get("v.currentPurchaseOrderIndex");
        debugger;
        var selectedBranch = component.get("v.selectedBranch")
        var nextIndex = (currentIndex + 50).toString();
        var action = component.get("c.next");
        helper.showSpinner(component, event);

        if(selectedBranch[0] != 'none'){
            action.setParams({
                "nextPage": nextIndex,
                "selectedBranch": null
            });
        } else {
            action.setParams({
                "nextPage": nextIndex,
                "selectedBranch": selectedBranch[0]
            });
        }

        action.setCallback(this, function(response){
        if(response.getState() === "SUCCESS"){
            component.set("v.purchaseOrders", response.getReturnValue());
            component.set("v.currentPurchaseOrderIndex", response.getReturnValue()[0].index);
        } else{
            errors.set('v.class', '');
            errors.get('v.body')[0].set("v.value", response.getError()[0].message);
        }
        helper.hideSpinner(component, event);
       });
       $A.enqueueAction(action);
      },

      previousPage : function(component, event, helper){
          var currentIndex = component.get("v.currentPurchaseOrderIndex");
          var selectedBranch = component.get("v.selectedBranch")
          var previousIndex = (currentIndex + 50).toString();
          var action = component.get("c.previous");
          helper.showSpinner(component, event);

          if(selectedBranch[0] == 'none'){
              action.setParams({
                  "previousPage": previousIndex,
                  "selectedBranch": null
              });
          } else {
              action.setParams({
                  "previousPage": previousIndex,
                  "selectedBranch": selectedBranch[0]
              });
          }

          action.setCallback(this, function(response){
              if(response.getState() === "SUCCESS"){
                  component.set("v.purchaseOrders", response.getReturnValue());
                  component.set("v.currentPurchaseOrderIndex", response.getReturnValue()[0].index);
                  component.set("v.purchaseOrderMax", false);
              } else{
                  errors.set('v.class', '');
                  errors.get('v.body')[0].set("v.value", response.getError()[0].message);
              }
              helper.hideSpinner(component, event);
             })
           $A.enqueueAction(action);
      },

      firstPage : function(component, event, helper){
          var action = component.get("c.next");
          var selectedBranch = component.get("v.selectedBranch")
          helper.showSpinner(component, event);


        if(selectedBranch[0] != 'none'){
            action.setParams({
                "nextPage": '0',
                "selectedBranch": null
            });
        } else {
            action.setParams({
                "nextPage": '0',
                "selectedBranch": selectedBranch[0]
            });
        }

          action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                component.set("v.purchaseOrders", response.getReturnValue());
                component.set("v.currentPurchaseOrderIndex", response.getReturnValue()[0].index);
                component.set("v.purchaseOrderMax", false);
            } else{
                errors.set('v.class', '');
                errors.get('v.body')[0].set("v.value", response.getError()[0].message);
            }
            helper.hideSpinner(component, event);
           })
          $A.enqueueAction(action);
      },

      lastPage : function(component, event, helper){
          var action = component.get("c.last");
          var totalPOSize = component.get("v.purchaseOrderSize");
          var selectedBranch = component.get("v.selectedBranch")
          helper.showSpinner(component, event);

          if(selectedBranch[0] != 'none'){
              action.setParams({
                  "selectedBranch": null
              });
          } else {
              action.setParams({
                  "selectedBranch": selectedBranch[0]
              });
          }
           action.setCallback(this, function(response){
              if(response.getState() === "SUCCESS"){
                  component.set("v.purchaseOrders", response.getReturnValue());
                  component.set("v.currentPurchaseOrderIndex", response.getReturnValue()[0].index);
                  component.set("v.purchaseOrderMax", true);
              } else{
                  errors.set('v.class', '');
                  errors.get('v.body')[0].set("v.value", response.getError()[0].message);
              }
              helper.hideSpinner(component, event);
             })
          $A.enqueueAction(action);
      },

    selectBranch : function(component, event, helper){
        var selectedBranch = component.get("v.selectedBranch")
        var action = component.get("c.next");

        if(selectedBranch[0]=="none"){
            helper.showSpinner(component, event);
            action.setCallback(this, function(response){
                if (response.getState() === "SUCCESS") {
                    component.set('v.purchaseOrders', []);
                    component.set("v.purchaseOrders", response.getReturnValue());
                    component.set("v.currentPurchaseOrderIndex", response.getReturnValue()[0].index);
                } else {
                    var errors = component.find("errors");
                    errors.set('v.class', '');
                    errors.get('v.body')[0].set("v.value", response.getError()[0].message);
                }
                helper.hideSpinner(component, event);
            });

        } else {
            helper.showSpinner(component, event);
            action.setParams({
                "nextPage": '0',
                "selectedBranch": selectedBranch[0]
            })
            action.setCallback(this, function(response){
                if (response.getState() === "SUCCESS") {
                    component.set('v.purchaseOrders', []);
                    component.set("v.purchaseOrders", response.getReturnValue());
                    component.set("v.currentPurchaseOrderIndex", response.getReturnValue()[0].index);
                } else {
                    var errors = component.find("errors");
                    errors.set('v.class', '');
                    errors.get('v.body')[0].set("v.value", response.getError()[0].message);
                }
                helper.hideSpinner(component, event);
            });
        }
        $A.enqueueAction(action);
    },

    deletePO : function (component, event, helper){
        var poToBeDeleted = event.getParam("poToBeDeleted");
        var posNotDeleted = component.get("v.purchaseOrders");

        posNotDeleted = posNotDeleted.filter(function(n){
            return n.purchaseOrder.Purchase_Order_Number__c != poToBeDeleted
            })

       var toastEvent = $A.get("e.force:showToast");
       toastEvent.setParams({
           "title": "Success!",
           "message": "The purchase order has been deleted."
       })
       toastEvent.fire();
       component.set("v.purchaseOrders", posNotDeleted);
    },
});