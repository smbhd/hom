({
	doInit : function(component, event, helper) {
        helper.loadData(component,event);
	},

	getMeasurements: function(component,event,helper){
	    helper.getMeasurements(component,event);
    },

    handleChildCreate : function(component,event,helper){
        helper.showMeasure(component,event);
    },
    handleInsert: function(component,event,helper){
        helper.handleChildInsert(component,event,helper);
    }


})