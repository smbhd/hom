/**
 * Created by HabibKhan on 5/17/2017.
 */
({
    loadData: function(cmp, event) {


        var recId = cmp.get('v.recordId');

        var action = cmp.get("c.getMeasurement");
        console.log(recId);

        action.setParams({
            "poId": cmp.get('v.recordId') + ''
        });

        action.setCallback(this, function(rsp) {
            var state = rsp.getState();

            if (state === 'SUCCESS') {
                var retVal = rsp.getReturnValue();

                if (retVal !== null) {
                    cmp.set('v.showData', true);
                    cmp.set('v.showMeasurement', false);
                    cmp.set('v.measurementList', retVal);
                    cmp.set('v.disable',false);
                }
            } else {
                cmp.set('v.showMeasurement', false);
                cmp.set('v.measurement', null);
                cmp.set('v.showData', false);


            }
            var spinner = cmp.find('mySpinner');
            $A.util.toggleClass(spinner, "slds-hide");
        });


        var recId = cmp.get('v.recordId');
        var validationAction = cmp.get('c.validatePo');
                    validationAction.setParams({
                        'id': recId
                    });

                    validationAction.setCallback(this, function(response) {
                        if (response.getState() === 'SUCCESS') {
                            var retval = response.getReturnValue();
                            cmp.set('v.message', retval);

                            if(retval === null){
                               cmp.set('v.showError', false);
                            }else{
                               cmp.set('v.showError', true);
                            }
                        }
                    });

                    $A.enqueueAction(validationAction);



        $A.enqueueAction(action);
    },
    getMeasurements: function(cmp, event) {
        var measurements = cmp.get('v.measurementList');
        var haveLoaded = cmp.get("v.haveLoaded");

        //we've already gotten data from callout
        if (haveLoaded) {
            cmp.set('v.showMeasurement', false);
            cmp.set('v.showData', true);
            cmp.set('v.disable',true);
        } else {
                     var recId = cmp.get('v.recordId');




            var showError = cmp.get('v.showError');
            if (!showError) {

                var spinner = cmp.find('mySpinner');
                $A.util.toggleClass(spinner, "slds-hide");

                var action = cmp.get('c.getMeasurementsFromCallout');

                action.setParams({
                    Id: recId
                });

                action.setCallback(this, function(rsp) {
                    if (rsp.getState() === 'SUCCESS') {

                        var retVal = rsp.getReturnValue();

                        if (retVal !== null) {
                            var retArray = JSON.parse(retVal);
                            cmp.set('v.showData', true);
                            cmp.set('v.showMeasurement', false);
                            cmp.set('v.measurementList', retArray);
                            cmp.set('v.disable',true);
                            cmp.set('v.haveLoaded', true);
                        }

                    } else {
                        var error = 'There was an issue with getting measurements from measure comp';
                        cmp.set('v.message', error);
                        cmp.set('v.showError',true);
                        cmp.set('v.showSuccess',false);
                    }

                window.setTimeout(
                $A.getCallback(function(){
                cmp.set('v.showError',false);
                cmp.set('v.showSuccess',false);
                }),5000);

                    $A.util.toggleClass(spinner, "slds-hide");
                });


                $A.enqueueAction(action);
            }

        }


    },
    showMeasure: function(cmp, evnt) {
        var swt = evnt.getParam('switch');
        if (swt) {
            this.getMeasurements(cmp, event);
        } else {
            var measure = evnt.getParam('measure');
            cmp.set('v.measurement', measure);
            cmp.set('v.showData', true);
            cmp.set('v.showMeasurement', true);
        }

    },
    handleChildInsert: function(cmp,event){
        var showError = event.getParam('showError');
        var message = event.getParam('message');
        cmp.set('v.message', message);

        if(showError){
            cmp.set('v.showError',true);
            cmp.set('v.showSuccess',false);
        }else{
            cmp.set('v.showError',false);
            cmp.set('v.showSuccess',true);
        }

        window.setTimeout(
            $A.getCallback(function(){
                cmp.set('v.showError',false);
                cmp.set('v.showSuccess',false);
            }),5000);


    }

})