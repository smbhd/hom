/**
 * Created by HabibKhan on 5/19/2017.
 */
({
    informParent: function(component,event,helper){
        var compEvent = component.getEvent('informP');
        compEvent.setParam('switch',true);
        compEvent.fire();
    },
    insertMeasure: function(component,event,helper){
        var recordId = component.get('v.poId');
        var action = component.get('c.upsertMeasure');

        var measure = component.get('v.measure');

        if(measure.Id == null){
                    console.log(measure);
                    action.setParams({'poId': recordId,'measureJSON': JSON.stringify(measure)});

                    action.setCallback(this, function(response){

                        var compEvent = component.getEvent('InsertSuccess');

                        var message = '';
                        var showError = false;


                        if(response.getState() === 'SUCCESS'){
                            var retval = JSON.parse(response.getReturnValue());
                            if(retval.Id){
                                message = 'Success!';
                                showError = false;
                            }else{
                                message = 'Oh no! There was an error inserting this measure record.';
                                showError = True;
                            }
                        }else{
                            console.log('failure');
                        }


                        compEvent.setParam('message',message);
                        compEvent.setParam('showError',showError);
                        compEvent.fire();
                    });
        }else{
              var compEvent = component.getEvent('InsertSuccess');
                 var message = 'You cannot reInsert a measure twice';
                 var showError = true;
                 compEvent.setParam('message',message);
                 compEvent.setParam('showError',showError);
                 compEvent.fire();
        }




        $A.enqueueAction(action);


    }
})