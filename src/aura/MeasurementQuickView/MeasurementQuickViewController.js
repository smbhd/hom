/**
 * Created by HabibKhan on 5/19/2017.
 */
({
    informParent: function(component, event,helper){
        var compEvent = component.getEvent('informP');
        var val = event.getSource().get('v.value');
        compEvent.setParam('measure',val);
        if(val){
            compEvent.setParam('switch',false);
        }else{
            compEvent.setParam('switch',true);
        }

        compEvent.fire();
    }
})