/**
 * Created by AlexSanborn on 4/7/2017.
 */
({
    doInit : function(component, event, helper){
            var action = component.get("c.getReadyToScheduleChargebacks");
//            helper.showSpinner(component, event);
            action.setCallback(this, function(response){
//                debugger
                if (response.getState() === "SUCCESS") {
                    component.set("v.chargebacks", response.getReturnValue());
                } else {
                    var errors = component.find("errors");
                    errors.set('v.class', '');
                    errors.get('v.body')[0].set("v.value", response.getError()[0].message);

                }
//                helper.hideSpinner(component, event);
            });

            $A.enqueueAction(action);
        },

        addSchedule : function(component, event, helper){
           var chargeback = event.getSource().get('v.value');
           var modal = document.querySelector('.slds-modal.schedule');
                       modal.classList.add('slds-fade-in-open');

           var backdrop = document.querySelector('.slds-backdrop');
                         backdrop.classList.add('slds-backdrop--open');

           component.set("v.chargebackToAdd", chargeback);
        },

        closeModal: function(component, event, helper) {
                var modalPreview = document.querySelector('.slds-modal.chargeback-preview');
                modalPreview.classList.remove('slds-fade-in-open');
                var modalSchedule = document.querySelector('.slds-modal.schedule');
                                modalSchedule.classList.remove('slds-fade-in-open');
                var backdrop = document.querySelector('.slds-backdrop');
                backdrop.classList.remove('slds-backdrop--open');

                //close modal = rerender page?
         },

         createChargeback : function(component, event, helper){
             var scheduleAction = component.get("c.scheduleChargeback");
             var rerenderAction = component.get("c.getReadyToScheduleChargebacks");

             var chargeback = event.getSource().get("v.value");

             var date = component.find("date").get("v.value");
             if(date === Array){
                 date = date[date.length - 1];
             }
             var installmentsNum = component.find("installments").get("v.value");
             if(installmentsNum === Array){
                  installmentsNum = installmentsNum[installmentsNum.length - 1];
             }
             var intervalsNum = component.find("intervals").get("v.value");
             if(intervalsNum === Array){
               intervalsNum = intervalsNum[intervalsNum.length - 1];
             }
             var chargebackJSON = JSON.stringify(chargeback);

             scheduleAction.setParams({
                 chargebackJSON: chargebackJSON,
                 numInstallments: installmentsNum,
                 installationDate: date,
                 intervals: intervalsNum
             })

             var scheduleModal = document.querySelector('.slds-modal.schedule');
                             scheduleModal.classList.remove('slds-fade-in-open');
             var previewModal = document.querySelector('.slds-modal.chargeback-preview');
                             previewModal.classList.add('slds-fade-in-open');

             scheduleAction.setCallback(this, function(response){

             //                debugger
                             if (response.getState() === "SUCCESS") {
                                 component.set("v.chargebackPreview", response.getReturnValue());
                             } else {
                                 var errors = component.find("errors");
                                 errors.set('v.class', '');
                                 errors.get('v.body')[0].set("v.value", response.getError()[0].message);

                             }
             //                helper.hideSpinner(component, event);
                         });

             $A.enqueueAction(scheduleAction);

             rerenderAction.setCallback(this, function(response){
                             if (response.getState() === "SUCCESS") {
                                 debugger;
                                 component.set("v.chargebacks", response.getReturnValue());
                             } else {
                                 var errors = component.find("errors");
                                 errors.set('v.class', '');
                                 errors.get('v.body')[0].set("v.value", response.getError()[0].message);

                             }
                         });
              $A.enqueueAction(rerenderAction);
         }
})