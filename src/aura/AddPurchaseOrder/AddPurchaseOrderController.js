/**
 * Created by AlexSanborn on 5/1/2017.
 */
({
    doInit : function(component, event, helper){
            var action = component.get("c.getAllOpenPurchaseOrdersAndLineItems");
            var projectId = component.get("v.recordId");

            action.setParams({
                "projectId": projectId
            });

            action.setCallback(this, function(response){
                if (response.getState() === "SUCCESS") {
                    component.set("v.purchaseOrders", response.getReturnValue());

                } else {
                    var errors = component.find("errors");
                    errors.set('v.class', '');
                    errors.get('v.body')[0].set("v.value", response.getError()[0].message);
                }
            });

            $A.enqueueAction(action);
        },
})