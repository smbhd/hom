/**
 * Created by AlexSanborn on 3/29/2017.
 */
({

        showSpinner: function (cmp, event) {
            'use strict';
            cmp.set('v.isLoading', true);
        },
        hideSpinner: function (cmp, event) {
            'use strict';
            cmp.set('v.isLoading', false);
        }
})