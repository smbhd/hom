/**
 * Created by AlexSanborn on 5/2/2017.
 */
({
    toggleLineItems: function(component, event, helper) {
                var target = event.target;
                while (target !== undefined && target.nodeName !== 'TR') {
                    target = target.parentNode;
                }
                if (target === undefined || target.classList.contains('lineItem')) {
                    return;
                }
                var lineItems = target.nextSibling;
                if (lineItems.classList.contains('hidden')) {
                    lineItems.classList.remove('hidden');
                } else {
                    lineItems.classList.add('hidden');
                }
            },
    addPO: function(component, event, helper){
        var purchaseOrderToAdd = component.get('v.purchaseOrder');
        var projectId = component.get("v.recordId");
        var action = component.get("c.addPurchaseOrder");
        debugger;


        action.setParams({
            "projectId": projectId,
            "purchaseOrderId": purchaseOrderToAdd.purchaseOrder.Id
        })





        action.setCallback(this, function(response){
                        if (response.getState() === "SUCCESS") {
                            component.set("v.added", true);
                        } else {
                            var errors = component.find("errors");
                            errors.set('v.class', '');
                            errors.get('v.body')[0].set("v.value", response.getError()[0].message);
                        }
                    });

                    $A.enqueueAction(action);
    }

})