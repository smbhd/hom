<!--
 - Created by AlexSanborn on 4/25/2017.
 -->

<aura:component description="PurchaseOrderRow" controller="PurchaseOrderController">
    <aura:attribute name="purchaseOrder" type="Map"/>
    <aura:attribute name="addressResults" type="Map[]" default="[]" />
    <aura:attribute name="zipcode" type="String" default="" />
    <aura:attribute name="zipcodeError" type="Boolean" default="false" />
    <aura:attribute name="phoneError" type="Boolean" default="false" />
    <aura:attribute name="viewLineItems" type="Boolean" default="false" />
    <aura:attribute name="viewAddressResults" type="Boolean" default="false" />
    <aura:attribute name="viewAddressInputs" type="Boolean" default="false" />
    <aura:attribute name="viewLineItemInputs" type="Boolean" default="false" />
    <aura:attribute name="requiredAddressFields" type="Boolean" default="false" />

    <aura:registerEvent name="deleteThisPO" type="c:DeletePurchaseOrder" />

    <tr id="{#v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c}" class="{! v.purchaseOrder.hasError ? 'hasError' : 'noError'}" onclick="{!c.toggleLineItems}">
            <td scope="row" data-label="Purchase Order Number">
                <div title="Purchase Order Number">{#v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c}</div>
            </td>
            <td scope="row" data-label="Branch">
                <div title="Branch">{#v.purchaseOrder.account.Branch_Number__c}</div>
            </td>
            <td scope="row" data-label="First Name">
                <div title="First Name">{#v.purchaseOrder.contact.FirstName}</div>
            </td>
            <td scope="row" data-label="Last Name">
                <div title="Last Name">{#v.purchaseOrder.contact.LastName}</div>
            </td>
            <td scope="row" data-label="Street Address">
                <div title="Street Address">
                {!v.purchaseOrder.account.BillingStreet}
                    <aura:if isTrue="{!v.viewAddressInputs}">
                        <ui:inputText value="{!v.purchaseOrder.billingStreet}"/>
                    </aura:if>
                </div>
            </td>
            <td scope="row" data-label="City">
                <div title="City">
                {!v.purchaseOrder.account.BillingCity}
                    <aura:if isTrue="{!v.viewAddressInputs}">
                        <ui:inputText value="{!v.purchaseOrder.billingCity}"/>
                    </aura:if>
                </div>
            </td>
            <td scope="row" data-label="State">
                <div title="State">
                {!v.purchaseOrder.account.BillingState}
                    <aura:if isTrue="{!v.viewAddressInputs}">
                        <ui:inputText value="{!v.purchaseOrder.billingState}"/>
                    </aura:if>
                </div>
            </td>
            <td scope="row" data-label="Postal Code">
                <div title="Postal Code">
                {!v.purchaseOrder.account.BillingPostalCode}
                    <aura:if isTrue="{!v.viewAddressInputs}">
                        <ui:inputtext value="{!v.purchaseOrder.billingPostalCode}" />
                    </aura:if>
                </div>
                <aura:if isTrue="{!v.zipcodeError}">
                    <p style="font-size:9px;color:red">Zip code is too long.</p>
                </aura:if>

            </td>
            <td scope="row" data-label="Phone">
                <div title="Phone">
                {!v.purchaseOrder.contact.Phone}
                    <aura:if isTrue="{!v.viewAddressInputs}">
                        <ui:inputtext value="{!v.purchaseOrder.phone}" />
                    </aura:if>

                </div>
                <div title="Phone">
                {!v.purchaseOrder.contact.Alternate_Phone__c}
                    <aura:if isTrue="{!v.viewAddressInputs}">
                        <ui:inputtext value="{!v.purchaseOrder.altPhone}" />
                    </aura:if>
                </div>

                <aura:if isTrue="{!v.phoneError}">
                    <p style="font-size:9px;color:red">One or both of the phone numbers are too long.</p>
                </aura:if>

            </td>
            <aura:if isTrue="{!v.requiredAddressFields}">
                <p style="font-size:9px;color:red">Please make sure street address, city, and state are filled in before saving address.</p>
            </aura:if>
            <td scope="row" data-label="Email">
                <div title="Email">{!v.purchaseOrder.contact.Email}</div>
            </td>
        <td scope="row" data-label="Action">


            <aura:if isTrue="{! !v.viewAddressInputs }">
                <lightning:buttonIcon value="{!v.purchaseOrder}"
                                      iconName="utility:edit"
                                      title="Edit"
                                      size="medium"
                                      variant="bare"
                                      onclick="{!c.poEdit}"
                                      class="poEditButton"/>
            </aura:if>
            <aura:if isTrue="{!v.viewAddressInputs}">
                <lightning:buttonIcon value="{!v.purchaseOrder}"
                                       iconName="utility:close"
                                       title="Close"
                                       size="medium"
                                       variant="bare"
                                       onclick="{!c.poCancelEdit}"
                                       class="poCancelEditButton"/>
            </aura:if>
            <aura:if isTrue="{!v.viewAddressInputs}">
                <lightning:buttonIcon value="{!v.purchaseOrder}"
                                      iconName="utility:check"
                                      title="Save"
                                      size="medium"
                                      variant="bare"
                                      onclick="{!c.poValidate}"
                                      class="poCancelEditButton"/>
            </aura:if>



        </td>
        <td scope="row" data-label="Create Project">
            <aura:if isTrue="{!v.purchaseOrder.hasError}">
                <div class="hasError">
                    <p>Cannot create a project for the following reasons:</p>
                    <aura:iteration items="{!v.purchaseOrder.errorMessages}" var="errorMessage">
                        <li>{!errorMessage}</li>
                    </aura:iteration>
                </div>
                <aura:set attribute="else">
                    <aura:if isTrue="{!v.purchaseOrder.verifiedAddress}">
                        <aura:if isTrue="{!v.purchaseOrder.hasProjectId}">
                            <a href="{! '#/sObject/' + v.purchaseOrder.projectId +'/view'}">Go to project</a>
                            <aura:set attribute="else">
                                <lightning:button value="{!v.purchaseOrder}"
                                                  label="Create Project"
                                                  variant="brand"
                                                  iconPosition="left"
                                                  onclick="{!c.handleSubmit}"/>
                            </aura:set>
                        </aura:if>
                        <aura:set attribute="else">
                            <lightning:button value="{!v.purchaseOrder}"
                                              label="Verify Address"
                                              variant="brand"
                                              iconPosition="left"
                                              onclick="{!c.addressList}"/>
                        </aura:set>
                    </aura:if>
                </aura:set>
            </aura:if>
            <aura:if isTrue="{!v.viewAddressResults}">
                <div style="color:rgb(22, 50, 92)" class="slds-modal slds-fade-in-show" id="{!globalId + '_addressesModal'}">
                    <div class="slds-modal__container">
                        <div class="slds-modal__header">
                            <div align="right">
                                <lightning:button title="Close"
                                        label="X"
                                        onclick="{!c.closeModal}" />
                            </div>
                            <h2 id="header43" class="slds-text-heading--medium">Select Address</h2>
                        </div>
                        <div class="slds-modal__content slds-p-around--medium">
                            <aura:iteration items="{!v.addressResults}" var="addressResult">
                                <div style="padding:5px;">
                                    <lightning:button value="{!addressResult}"
                                                      label="{!addressResult.street + ' ' + addressResult.city + ', ' + addressResult.state + ' ' + addressResult.zip}"
                                                      variant="brand"
                                                      iconPosition="left"
                                                      onclick="{!c.poSave}"
                                    />

                                </div>
                            </aura:iteration>


                            <aura:if isTrue="{!v.addressResults.length == 0 }">
                                <p> No matches were found. Use this address anyways: </p>
                                <div style="padding:5px;">
                                    <lightning:button value="{!v.purchaseOrder}"
                                                      label="{!v.purchaseOrder.billingStreet + ' ' + v.purchaseOrder.billingCity + ', ' + v.purchaseOrder.billingState + ' ' + v.purchaseOrder.billingZip}"
                                                      variant="brand"
                                                      iconPosition="left"
                                                      onclick="{!c.poSave}"
                                    />
                                </div>

                            </aura:if>

                            <div style="padding-top:10px" align="right">
                                <lightning:button label="Cancel"
                                                  iconPosition="right"
                                                  onclick="{!c.closeModal}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slds-backdrop slds-backdrop--show" id="{!globalId + '_addressesBackdrop'}"></div>
            </aura:if>




        </td>

        <td scope="row" data-label="Reject PO">
            <lightning:buttonIcon value="{!v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c}"
                                  iconName="utility:delete"
                                  title="Delete"
                                  size="medium"
                                  variant="bare"
                                  onclick="{!c.rejectPurchaseOrder}" />
        </td>
    </tr>
    <aura:if isTrue="{!v.viewLineItems}">
        <tr class="lineItems">
            <td colspan="12">
                <table class="slds-table slds-table--cell-buffer">
                    <thead>
                    <tr class="slds-text-title--caps slds-table--bordered" style="background: #f6f6f6;">
                        <th scope="col">
                            <div class="slds-truncate" title="Description">Description</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" title="Quantity">Quantity</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" title="Unit Price">Unit Price</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" title="Unit of Measure">Unit of Measure</div>
                        </th>
                        <th scope="col">&nbsp;</th>
                        <th scope="col" style="width: 250px;">
                            <div class="slds-truncate" title="Error Message">Error Message</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <aura:iteration items="{!v.purchaseOrder.purchaseOrderLines}" var="lineItem" indexVar="index">
                        <tr id="{!v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c + '-' + index}" class="{!lineItem.lineItemHasError ? 'hasError' : 'noError'}">
                            <td scope="row" data-label="Description">
                                <div title="Description">{#lineItem.purchaseOrderLine.Item_Name__c}</div>
                            </td>
                            <td scope="row" data-label="Quantity">
                                <div title="Quantity">
                                    <ui:outputText value="{!lineItem.purchaseOrderLine.Quantity__c}"/>
                                    <aura:if isTrue="{!v.viewLineItemInputs}">
                                        <ui:inputText value="{!lineItem.quantity}"/>
                                    </aura:if>
                                </div>
                            </td>
                            <td scope="row" data-label="Unit Cost">
                                <div title="Unit Price">
                                    <ui:outputText value="{!lineItem.purchaseOrderLine.Unit_Price__c}"/>
                                    <aura:if isTrue="{!v.viewLineItemInputs}">
                                        <ui:inputText value="{!lineItem.unitPrice}"/>
                                    </aura:if>
                                </div>
                            </td>
                            <td scope="row" data-label="Unit of Measure">
                                <div title="Unit of Measure">{!lineItem.purchaseOrderLine.Unit_of_Measure__c}</div>
                            </td>

                            <td scope="row" data-label="Action">

                                <aura:if isTrue="{! !v.viewLineItemInputs }">
                                    <lightning:buttonIcon value="{!lineItem}"
                                                          iconName="utility:edit"
                                                          title="Edit"
                                                          size="medium"
                                                          variant="bare"
                                                          onclick="{!c.liEdit}"
                                                          class="{!v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c + '-' + index + ' liEditButton'}"/>
                                </aura:if>
                                <lightning:buttonIcon value="{!v.purchaseOrder.purchaseOrderLines}"
                                                      iconName="utility:delete"
                                                      title="Delete"
                                                      size="medium"
                                                      variant="bare"
                                                      onclick="{!c.liDelete}"
                                                      class="{!v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c + '-' + index}"/>
                                <aura:if isTrue="{!v.viewLineItemInputs}">
                                    <lightning:buttonIcon value="{!lineItem}"
                                                          iconName="utility:close"
                                                          title="Close"
                                                          size="medium"
                                                          variant="bare"
                                                          onclick="{!c.liCancelEdit}"
                                                          class="{!v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c + '-' + index + ' liCancelEditButton'}"/>
                                    <lightning:buttonIcon value="{!lineItem}"
                                                          iconName="utility:check"
                                                          title="Save"
                                                          size="medium"
                                                          variant="bare"
                                                          onclick="{!c.liSave}"
                                                          class="{!v.purchaseOrder.purchaseOrder.Purchase_Order_Number__c + '-' + index + ' liSaveButton'}"/>
                                </aura:if>
                                <lightning:button value="{!lineItem}"
                                                  label="Override"
                                                  variant="brand"
                                                  iconPosition="left"
                                                  onclick="{!c.liSave}"/>
                            </td>
                            <td scope="row" data-label="Error Message">
                                <div class="{!lineItem.lineItemHasError ? 'hasError' : 'noError'}" title="Error Message">{!lineItem.lineItemErrorMessage}</div>
                            </td>
                        </tr>
                    </aura:iteration>
                    </tbody>
                </table>
            </td>
        </tr>
    </aura:if>
</aura:component>