/**
 * Created by AlexSanborn on 4/26/2017.
 */
({
     handleSubmit : function(component, event, helper){
            var action = component.get("c.createNewProject");
            var purchaseOrder = component.get('v.purchaseOrder');
            var purchaseOrderJSON = JSON.stringify(purchaseOrder);
            console.log(purchaseOrderJSON);
//            helper.showSpinner(component, event);

            action.setParams({
                "purchaseOrderJSON": purchaseOrderJSON
            });

            action.setCallback(this, function(response){
                purchaseOrder.hasProjectId = true;

                purchaseOrder.projectId = response.getReturnValue()

                component.set("v.purchaseOrder", purchaseOrder);
                component.set("v.purchaseOrder.projectId", purchaseOrder.projectId);
//                helper.showSpinner(component, event);
            });

           $A.enqueueAction(action);
        },

        poEdit: function(component, event, helper){
            debugger;
            component.set("v.viewAddressInputs", true);
        },

        poCancelEdit: function(component, event, helper) {
            component.set("v.viewAddressInputs", false);
        },

        poSave: function(component, event, helper) {
            var apiResults = component.get("v.addressResults");
            debugger;
            var selectedAddress = event.getSource().get('v.value');
            var purchaseOrderWrapper = component.get('v.purchaseOrder');

            component.set("v.viewAddressInputs", false);

            if(apiResults.length == 0){
                purchaseOrderWrapper.contact.MailingStreet = purchaseOrderWrapper.billingStreet
                purchaseOrderWrapper.contact.MailingCity = purchaseOrderWrapper.billingCity
                purchaseOrderWrapper.contact.MailingState = purchaseOrderWrapper.billingState
                purchaseOrderWrapper.contact.MailingPostalCode = purchaseOrderWrapper.billingPostalCode
                purchaseOrderWrapper.account.Name = purchaseOrderWrapper.billingStreet;
                purchaseOrderWrapper.account.BillingStreet = purchaseOrderWrapper.billingStreet
                purchaseOrderWrapper.account.BillingCity = purchaseOrderWrapper.billingCity
                purchaseOrderWrapper.account.BillingState = purchaseOrderWrapper.billingState
                purchaseOrderWrapper.account.BillingPostalCode = purchaseOrderWrapper.billingPostalCode
                purchaseOrderWrapper.billingStreet = purchaseOrderWrapper.billingStreet
                purchaseOrderWrapper.billingCity = purchaseOrderWrapper.billingCity
                purchaseOrderWrapper.billingState = purchaseOrderWrapper.billingState
                purchaseOrderWrapper.billingPostalCode = purchaseOrderWrapper.billingPostalCode
                purchaseOrderWrapper.verifiedAddress = true;

            } else {
                purchaseOrderWrapper.contact.MailingStreet = selectedAddress.street
                purchaseOrderWrapper.contact.MailingCity = selectedAddress.city
                purchaseOrderWrapper.contact.MailingState = selectedAddress.state;
                purchaseOrderWrapper.contact.MailingPostalCode = selectedAddress.zip;
                purchaseOrderWrapper.account.Name = selectedAddress.street;
                purchaseOrderWrapper.account.BillingStreet = selectedAddress.street
                purchaseOrderWrapper.account.BillingCity = selectedAddress.city
                purchaseOrderWrapper.account.BillingState = selectedAddress.state;
                purchaseOrderWrapper.account.BillingPostalCode = selectedAddress.zip;
                purchaseOrderWrapper.billingStreet = selectedAddress.street
                purchaseOrderWrapper.billingCity = selectedAddress.city
                purchaseOrderWrapper.billingState = selectedAddress.state;
                purchaseOrderWrapper.billingPostalCode = selectedAddress.zip;
                purchaseOrderWrapper.verifiedAddress = true;

            }
            component.set('v.viewAddressResults', false);
            component.set("v.purchaseOrder", purchaseOrderWrapper);
        },

        poValidate : function (component, event, helper) {
            var selectedAddress = event.getSource().get('v.value');

            var addressErrors = false;
            if(selectedAddress.billingPostalCode.length > 10){
                addressErrors = true;
                component.set("v.zipcodeError", true);
            }

            if(selectedAddress.phone.length > 14 || selectedAddress.altPhone.length > 14){
                addressErrors = true;
                component.set("v.phoneError", true)
            }

            if(selectedAddress.billingStreet.length == 0 || selectedAddress.billingCity == 0 || selectedAddress.billingState == 0){
                addressErrors = true;
                component.set("v.requiredAddressFields", true);
            }

            if(!addressErrors){
                var purchaseOrderWrapper = component.get('v.purchaseOrder');
                component.set("v.viewAddressInputs", false);

                purchaseOrderWrapper.contact.MailingStreet = selectedAddress.billingStreet
                purchaseOrderWrapper.contact.MailingCity = selectedAddress.billingCity
                purchaseOrderWrapper.contact.MailingState = selectedAddress.billingState;
                purchaseOrderWrapper.contact.MailingPostalCode = selectedAddress.billingPostalCode;
                purchaseOrderWrapper.contact.Phone = selectedAddress.phone;
                purchaseOrderWrapper.contact.Alternate_Phone__c = selectedAddress.altPhone;
                purchaseOrderWrapper.account.Name = selectedAddress.billingStreet;
                purchaseOrderWrapper.account.BillingStreet = selectedAddress.billingStreet
                purchaseOrderWrapper.account.BillingCity = selectedAddress.billingCity
                purchaseOrderWrapper.account.BillingState = selectedAddress.billingState;
                purchaseOrderWrapper.account.BillingPostalCode = selectedAddress.billingPostalCode;
                purchaseOrderWrapper.account.Phone = selectedAddress.phone;
                purchaseOrderWrapper.billingStreet = selectedAddress.billingStreet
                purchaseOrderWrapper.billingCity = selectedAddress.billingCity
                purchaseOrderWrapper.billingState = selectedAddress.billingState;
                purchaseOrderWrapper.billingPostalCode = selectedAddress.billingPostalCode;
                purchaseOrderWrapper.phone = selectedAddress.phone;
                purchaseOrderWrapper.altPhone = selectedAddress.altPhone;
                purchaseOrderWrapper.verifiedAddress = false;
                component.set("v.zipcodeError", false);
                component.set("v.phoneError", false);
                component.set("v.requiredAddressFields", false);
                component.set("v.purchaseOrder", purchaseOrderWrapper);
            }
        },

        liEdit: function(component, event, helper) {
            component.set("v.viewLineItemInputs", true);

        },

        liCancelEdit: function(component, event, helper) {
            component.set("v.viewLineItemInputs", false);
        },

        liSave: function(component, event, helper) {
            var purchaseOrderLineWrapper = event.getSource().get('v.value');
            var wrapperToChange = component.get('v.purchaseOrder');
            component.set("v.viewLineItemInputs", false);
            debugger;
            // refresh the view
            purchaseOrderLineWrapper.purchaseOrderLine.Quantity__c = purchaseOrderLineWrapper.quantity;

              purchaseOrderLineWrapper.purchaseOrderLine.Unit_Price__c = purchaseOrderLineWrapper.unitPrice;
              purchaseOrderLineWrapper.lineItemHasError = false;
              purchaseOrderLineWrapper.lineItemErrorMessage = "";


              var hasError = false;
              for(var i=0; i < wrapperToChange.purchaseOrderLines.length; i++){
                  if (wrapperToChange.purchaseOrderLines[i].lineItemHasError == true){
                      hasError = true;
                  }
              }
              if (hasError == false){
                  wrapperToChange.hasError = false;
                  wrapperToChange.errorMessages = [];
              }
              component.set("v.purchaseOrder", wrapperToChange);
//
        },

        liDelete: function(component, event, helper) {
            var wrapperToChange = component.get('v.purchaseOrder')
            var lineItems = event.getSource().get('v.value');
            var lineItemToDelete = document.getElementById(event.getSource().get('v.class')).id.split('-')[1];
            var lineItemToDeleteInt = Number(lineItemToDelete);
            delete lineItems[lineItemToDeleteInt];
            lineItems = lineItems.filter(function(n) {return n != undefined});
            component.set("v.purchaseOrder.purchaseOrderLines", lineItems);

            var hasError = false;
            if(wrapperToChange.purchaseOrderLines.length == 0){
                hasError = true;
                component.set("v.purchaseOrder.errorMessages", ['Purchase order cannot be created without line items.']);
                component.set("v.purchaseOrder", wrapperToChange);
            } else {
                for(var i=0; i < wrapperToChange.purchaseOrderLines.length; i++){
                    if (wrapperToChange.purchaseOrderLines[i].lineItemHasError == true){
                        hasError = true;
                    }
                }
            }

            if (hasError == false){
                wrapperToChange.hasError = false;
                wrapperToChange.errorMessages = [];
                component.set("v.purchaseOrder", wrapperToChange);
            }
        },

        toggleLineItems: function(component, event, helper) {
            var toggle = component.get('v.viewLineItems');

            if(toggle == true){
                var change = false;
            } else{
                var change = true;
            }
            component.set('v.viewLineItems', change);
        },

        addressList: function(component, event, helper) {
            var action = component.get('c.getAddressList');
            var purchaseOrderWrapper = event.getSource().get('v.value');

             var addressErrors = false;
             if(purchaseOrderWrapper.billingPostalCode.length > 10){
                    addressErrors = true;
                    component.set("v.zipcodeError", true);
             }

             if(purchaseOrderWrapper.phone.length > 14 || purchaseOrderWrapper.altPhone.length > 14){
                    addressErrors = true;
                    component.set("v.phoneError", true)
             }

             if(purchaseOrderWrapper.billingStreet.length == 0 || purchaseOrderWrapper.billingCity == 0 || purchaseOrderWrapper.billingState == 0){
                     addressErrors = true;
                     component.set(v.requiredAddressFields, true);
              }

           console.log(purchaseOrderWrapper);
           if(!addressErrors){
                action.setParams({
                    "street": purchaseOrderWrapper.billingStreet.trim(),
                    "city": purchaseOrderWrapper.billingCity.trim(),
                    "state": purchaseOrderWrapper.billingState.trim(),
                    "postalcode": purchaseOrderWrapper.billingPostalCode.trim().slice(0,5)
                });

                action.setCallback(this, function(response) {
                    component.set("v.viewAddressResults", true)
                    component.set("v.zipcodeError", false);
                    component.set("v.phoneError", false);
                    component.set("v.requiredAddressFields", false);
                    component.set('v.addressResults', []);
                    component.set('v.addressResults', response.getReturnValue());
                });

                $A.enqueueAction(action);
            }
        },

        closeModal: function(component, event, helper) {
            component.set('v.viewAddressResults', false);
        },

        rejectPurchaseOrder : function(component, event, helper){
            var confirmToDelete = confirm("Are you sure you want to delete this purchase order? Deleting this purchase order will delete all purchase orders in the queue with this purchase order number.");

            if(confirmToDelete == true) {
                var action = component.get("c.rejectPOs");
                var purchaseOrderWrapper = component.get("v.purchaseOrder");
                var poNum = purchaseOrderWrapper.purchaseOrder.Purchase_Order_Number__c;
                var poToDelete = document.getElementById(event.getSource().get('v.value'));

                action.setParams({
                    "poNum" : poNum
                });

                action.setCallback(this, function(response){
                    if (response.getState() === "SUCCESS") {
                        if(response.getReturnValue() == 200){
                            var deletePO = component.getEvent("deleteThisPO");
                            debugger;
                            deletePO.setParams({
                                "poToBeDeleted" : poNum
                            })
                            deletePO.fire();
                        } else{
                            alert('Purchase Order could not be deleted from Home Depot database.')
                        }
                    }
                        else {
                        var errors = component.find("errors");
                        errors.set('v.class', '');
                        errors.get('v.body')[0].set("v.value", response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action);
            }


        }
})