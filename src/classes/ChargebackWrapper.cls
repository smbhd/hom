/**
 * Created by AlexSanborn on 4/7/2017.
 */

global with sharing class ChargebackWrapper {
    @AuraEnabled
    global CKSW_BASE__Service_Order__c project {get;set;}
    @AuraEnabled
    global Boolean hasLineItems {get;set;}
    @AuraEnabled
    global CKSW_BASE__Service_Product__c lineItem {get;set;}
    @AuraEnabled
    global Chargeback_Rate__c chargebackRate {get;set;}
    @AuraEnabled
    global String itemName {get;set;}
    @AuraEnabled
    global String contractorName {get;set;}
    @AuraEnabled
    global String projectName {get; set;}

}
