/**
 * Created by AlexSanborn on 5/9/2017.
 */

@isTest
public class ChargebackControllerTest {
//    @testSetup
//    public static void setup(){
//        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
//        List<SKU_Price__c> prices = new List<SKU_Price__c>();
//        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
//        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();
//
//        Account newAccount = new Account(Name='test acct');
//        insert newAccount;
//
//        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
//        insert newProject;
//
//        RecordType chargebackRecordType = [SELECT id FROM RecordType
//        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];
//
//        Contractor_Company2__c contractorCompany = new Contractor_Company2__c(Name='The contractors');
//        insert contractorCompany;
//
//        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
//        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
//        skus.add(sku1);
//        skus.add(sku2);
//        insert skus;
//
//        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
//        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
//        skuProducts.add(skuProduct1);
//        skuProducts.add(skuProduct2);
//        insert skuProducts;
//
//
//        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
//        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
//        prices.add(skuprice1);
//        prices.add(skuprice2);
//        insert prices;
//
//        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
//        newService1.CKSW_BASE__Service_Order__c = newProject.id;
//        newService1.CKSW_BASE__Status__c = 'New';
//        newService1.Contractor_Company2__c = contractorCompany.id;
//        newService1.CKSW_BASE__Service_Type__c = sku1.id;
//        insert newService1;
//
//        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
//                RecordTypeId=chargebackRecordType.id,
//                Initial_Work_Order__c = newService1.id,
//                CKSW_BASE__Status__c='Close',
//                Chargeback_Status__c='Ready to Schedule'
//        );
//        insert newChargebackServiceOrder;
//
//        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
//        insert newPOChargeback;
//
//        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
//        insert newLI1;
//
//        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
//                CKSW_BASE__Quantity_Used__c=5,
//                Service_Units__c='Square Foot',
//                CKSW_BASE__Service__c=newService1.id,
//                SKU__c=sku1.id,
//                Purchase_Order_Line__c=newLI1.id,
//                Service_Unit_Price__c=4.00);
//        insert newWOLI1;
//
//        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
//        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
//        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
//        chargebackRates.add(newChargebackRate3);
//        chargebackRates.add(newChargebackRate2);
//        chargebackRates.add(newChargebackRate1);
//        insert chargebackRates;
//    }

    @isTest
    public static void findsAndReturnsInfoToScheduleChargebacks(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Ready to Schedule'
        );
        insert newChargebackServiceOrder;

        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);
        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;

        List<ChargebackWrapper> chargeBacks = ChargebackController.getReadyToScheduleChargebacks();
        ChargebackWrapper chargeback = chargeBacks.get(0);

        System.assertEquals(chargeback.project.id, newChargebackServiceOrder.id);
    }

    @IsTest
    public static void flagsIfThereAreNoLineItems(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Ready to Schedule'
        );
        insert newChargebackServiceOrder;

        List<ChargebackWrapper> chargeBacks = ChargebackController.getReadyToScheduleChargebacks();
        ChargebackWrapper chargeback = chargeBacks.get(0);
        System.assertEquals(chargeback.hasLineItems, FALSE);
    }

    @IsTest
    public static void createsChargebackPreviewWrapperForWeekly(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Ready to Schedule'
        );
        insert newChargebackServiceOrder;

        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);
        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;

        List<ChargebackWrapper> chargeBacks = ChargebackController.getReadyToScheduleChargebacks();
        ChargebackWrapper chargeback = chargeBacks.get(0);
        String chargebackJSON = JSON.serialize(chargeback);
        String dateString = '2050-05-10';
        Date d = date.newInstance(2050, 5, 10);
        ChargebackPreviewWrapper preview = ChargebackController.scheduleChargeback(chargebackJSON, '5', dateString, 'Weekly');


        Test.startTest();
        CKSW_BASE__Service_Order__c updatedProject = [
                SELECT id, Number_of_Installments__c, First_Installment_Date__c,
                        Next_Installment_Date__c, Interval__c, Chargeback_Status__c
                FROM CKSW_BASE__Service_Order__c
                WHERE RecordTypeId=:chargebackRecordType.id];

        System.assertEquals(5, updatedProject.Number_of_Installments__c);
        System.assertEquals(d, updatedProject.First_Installment_Date__c);
        System.assertEquals(d, updatedProject.Next_Installment_Date__c);
        System.assertEquals('Weekly', updatedProject.Interval__c);
        System.assertEquals('Schedule Complete', updatedProject.Chargeback_Status__c);
        System.assertEquals(preview.ItemName, 'flooring');
        System.assertEquals(preview.contractorName, 'The contractors');
        System.assertEquals(preview.amtRecurrence, 4.00);
        System.assertEquals(preview.chargebackRate.Name, 'CR100008');
        Test.stopTest();
    }

    @isTest
    public static void createsChargebackPreviewWrapperForMonthly(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Ready to Schedule'
        );
        insert newChargebackServiceOrder;

        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);
        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;

        List<ChargebackWrapper> chargeBacks = ChargebackController.getReadyToScheduleChargebacks();
        ChargebackWrapper chargeback = chargeBacks.get(0);
        String chargebackJSON = JSON.serialize(chargeback);
        String dateString = '2050-05-10';
        Date d = date.newInstance(2050, 5, 10);
        ChargebackPreviewWrapper preview = ChargebackController.scheduleChargeback(chargebackJSON, '5', dateString, 'Monthly');

        Test.startTest();
        CKSW_BASE__Service_Order__c updatedServiceOrder = [
                SELECT id, Number_of_Installments__c, First_Installment_Date__c,
                        Next_Installment_Date__c, Interval__c, Chargeback_Status__c
                FROM CKSW_BASE__Service_Order__c
                WHERE RecordTypeId=:chargebackRecordType.id
                LIMIT 1];

        System.assertEquals(5, updatedServiceOrder.Number_of_Installments__c);
        System.assertEquals(d, updatedServiceOrder.First_Installment_Date__c);
        System.assertEquals(d, updatedServiceOrder.Next_Installment_Date__c);
        System.assertEquals('Monthly', updatedServiceOrder.Interval__c);
        System.assertEquals('Schedule Complete', updatedServiceOrder.Chargeback_Status__c);
        System.assertEquals(preview.ItemName, 'flooring');
        System.assertEquals(preview.contractorName, 'The Contractors');
        System.assertEquals(preview.amtRecurrence, 4.00);
        System.assertEquals(preview.chargebackRate.Name, 'CR100008');
        Test.stopTest();
    }
}