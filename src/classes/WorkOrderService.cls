/**
 * Created by AlexSanborn on 5/3/2017.
 */

public with sharing class WorkOrderService {
    public static void createPayoutWhenStatusClosed(List<CKSW_BASE__Service__c> closedServices){

        List<CKSW_BASE__Service__c> payoutWorkOrders = new List<CKSW_BASE__Service__c>();
        List<Id> payoutWorkOrderIds = new List<Id>();
        List<PayoutChargeback__c> newPayouts = new List<PayoutChargeback__c>();
        List<Id> serviceOrderIds = new List<Id>();

        System.debug(closedServices);

        for(CKSW_BASE__Service__c service :closedServices){
            serviceOrderIds.add(service.CKSW_BASE__Service_Order__c);
        }

        RecordType warrantyRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'CKSW_Base__Service_Order__c' AND DeveloperName='Warranty_Project'];
        List<CKSW_BASE__Service_Order__c> serviceOrders = [SELECT id, RecordTypeId FROM CKSW_BASE__Service_Order__c WHERE id IN :serviceOrderIds];

        for(CKSW_BASE__Service__c service : closedServices){
            if(service.CKSW_BASE__Status__c == 'Work Complete'){
                for(CKSW_BASE__Service_Order__c so : serviceOrders){
                    System.debug(so);
                    System.debug(service);
                    System.debug(warrantyRecordType);
                    if(service.CKSW_BASE__Service_Order__c == so.id && so.RecordTypeId != warrantyRecordType.id){
                        payoutWorkOrders.add(service);
                        payoutWorkOrderIds.add(service.id);
                    }
                }

            }
        }


        if(!payoutWorkOrders.isEmpty()){
            //grab payoutPrices depending on which products exist in list
            List<CKSW_BASE__Service_Product__c> lineItems = [SELECT id, CKSW_BASE__Quantity_Used__c, Service_Units__c,
                    Service_Unit_Price__c, SKU__c, Purchase_Order_Line__r.Item_Name__c,
                    CKSW_BASE__Service__r.Contractor_Company2__c, CKSW_BASE__Service__r.CKSW_BASE__Service_Order__c
            FROM CKSW_BASE__Service_Product__c WHERE CKSW_BASE__Service__c IN :payoutWorkOrderIds];

            List<id> skusFromLineItems = new List<id>();
            if(lineItems!=null){
                for(CKSW_BASE__Service_Product__c li : lineItems){
                    if(li.SKU__c != null){
                        skusFromLineItems.add(li.SKU__c);
                    }
                }
            }

            System.debug(lineItems);
            System.debug(skusFromLineItems);


            List<Payout_Price__c> payoutPrices = [SELECT id, Unit_Price__c, Unit_Type__c, Payout_Price_Type__c, SKU__c FROM Payout_Price__c WHERE SKU__c IN :skusFromLineItems];
            RecordType payoutRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'PayoutChargeback__c' AND DeveloperName='Payout'];

            //map skus to line items
            Map<CKSW_BASE__Service_Product__c, List<Payout_Price__c>> skusByLineItems = new Map <CKSW_BASE__Service_Product__c, List<Payout_Price__c>>();
            for(CKSW_BASE__Service_Product__c li : lineItems){
                if(li.SKU__c != null && !skusByLineItems.containsKey(li)){
                    List<Payout_Price__c> payoutPricesList = new List<Payout_Price__c>();
                    skusByLineItems.put(li, payoutPricesList);
                }

                if(skusByLineItems.containsKey(li)){
                    Id currentSKU = li.SKU__c;
                    for(Integer p=0; p<payoutPrices.size(); p++){
                        if(payoutPrices.get(p).SKU__c == currentSKU){
                            skusByLineItems.get(li).add(payoutPrices.get(p));
                        }
                    }
                }
            }

            System.debug(skusByLineItems);

            Set<CKSW_BASE__Service_Product__c> lineItemKeys = skusByLineItems.keySet();
            for(CKSW_BASE__Service_Product__c li : lineItemKeys){
                PayoutChargeback__c newPayout = new PayoutChargeback__c();
                newPayout.RecordTypeId = payoutRecordType.id;
                newPayout.Payout_Status__c = 'Current';
                newPayout.Quantity__c = li.CKSW_BASE__Quantity_Used__c;
                newPayout.Unit_of_Measure__c = li.Service_Units__c;
                newPayout.Project__c = li.CKSW_BASE__Service__r.CKSW_BASE__Service_Order__c;
                newPayout.Work_Order_Line__c = li.id;
                newPayout.Contractor_Company2__c = li.CKSW_BASE__Service__r.Contractor_Company2__c;

                List<Payout_Price__c> current = skusByLineItems.get(li);
                for(Integer c=0; c<current.size(); c++){
                    if(c==0 || current.get(c).Payout_Price_Type__c == 'General Contractor'){
                        newPayout.Payout_Price__c = current.get(c).id;
                        newPayout.Unit_Price__c = current.get(c).Unit_Price__c;
                    } else if(c>0 && current.get(c).Payout_Price_Type__c == 'Servicing Location'){
                        newPayout.Payout_Price__c = current.get(c).id;
                        newPayout.Unit_Price__c = current.get(c).Unit_Price__c;
                    }
                }
                newPayouts.add(newPayout);
            }
        }
        System.debug(newPayouts);

        insert newPayouts;
    }
}