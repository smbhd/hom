/**
 * Created by AlexSanborn on 5/17/2017.
 */

global class PurchaseOrderIterable implements Iterator <List<PurchaseOrderWrapper>> {

    List<PurchaseOrderWrapper> InnerList{get; set;}
    List<PurchaseOrderWrapper> ListRequested{get; set;}

    Integer i {get; set;}
    String branchNum {get;set;}
    public Integer setPageSize {get; set;}

    public PurchaseOrderIterable(List<PurchaseOrderWrapper> firstWrapper, Integer currentPage, String selectedBranch) {
        InnerList = new List<PurchaseOrderWrapper>();
        ListRequested = new List<PurchaseOrderWrapper>();
        InnerList = firstWrapper;
        System.debug(firstWrapper);
        System.debug(InnerList);
        setPageSize = 50;
        i = currentPage;
        branchNum = selectedBranch;
    }

    global boolean hasNext(){
        if(i >= InnerList.size()) {
            return false;
        } else {
            return true;
        }
    }

    global boolean hasPrevious(){
        system.debug('I am in hasPrevious' + i);
        if(i <= setPageSize) {
            return false;
        } else {
            return true;
        }
    }

    global list<PurchaseOrderWrapper> next(){
        system.debug('i value is ' + i);
        ListRequested = new List<PurchaseOrderWrapper>();
        integer startNumber;

        integer size = InnerList.size();
        if(hasNext()){
            if(size <= (i + setPageSize)){
                startNumber = i;
                i = size;
            }
            else {
                i = (i + setPageSize);
                startNumber = (i - setPageSize);
            }

            system.debug('i value is =====' + i);
            system.debug('i value is 2==== ' + (i - setPageSize));
            system.debug(startNumber);
            if(branchNum != null){
                for(integer start = startNumber; start < InnerList.size() && ListRequested.size() <= 50 ; start++) {
                    System.debug(InnerList[start]);
                    if(InnerList[start].branch.Branch_Number__c == branchNum){
                        ListRequested.add(InnerList[start]);
                    }
                }
            } else {
                for(integer start = startNumber; start < i; start++) {
                    ListRequested.add(InnerList[start]);
                }
            }
        }
        System.debug(ListRequested);
        return ListRequested;
    }

    global list<PurchaseOrderWrapper> previous(){
        ListRequested = new list<PurchaseOrderWrapper>();
        system.debug('i value is previous before =====' + i);
        integer size = InnerList.size();
        if(i == size) {
            if(math.mod(size, setPageSize) > 0) {
                i = size - math.mod(size, setPageSize);
            }
            else {
                i = (size - setPageSize);
            }
        }
        else {
            i = (i - setPageSize);
        }

        system.debug('i value is previous =====' + i);
        system.debug('i value is 2previous ==== ' + (i - setPageSize));

        if(branchNum != null){
            List<PurchaseOrderWrapper> wrappersReversed = new List<PurchaseOrderWrapper>();

            for(integer start = i; start > 0; start--) {
                if(InnerList[start].branch.Branch_Number__c == branchNum){
                    wrappersReversed.add(InnerList[start]);
                }
            }

            for(Integer po = 0; po < wrappersReversed.size() && ListRequested.size() <= 50; po++){
                ListRequested.add(wrappersReversed[po]);
            }

        } else {
            for(integer start = (i - setPageSize); start < i; ++start) {
                ListRequested.add(InnerList[start]);
            }
        }
        System.debug(ListRequested);
        return ListRequested;
    }

    global List<PurchaseOrderWrapper> last(){
        ListRequested = new list<PurchaseOrderWrapper>();
        system.debug('i value is previous before =====' + i);
        System.debug(InnerList);
        integer size = InnerList.size();

        system.debug('i value is previous =====' + i);
        system.debug('i value is 2previous ==== ' + (i - setPageSize));

        if(branchNum != null) {
            List<PurchaseOrderWrapper> wrappersReversed = new List<PurchaseOrderWrapper>();
            for(integer start = InnerList.size(); start > 0; start--) {
                if(InnerList[start].branch.Branch_Number__c == branchNum){
                    wrappersReversed.add(InnerList[start]);
                }
            }
            for(Integer po = 0; po < wrappersReversed.size() && ListRequested.size() <= 50; po++){
                ListRequested.add(wrappersReversed[po]);
            }

        } else {
            for(integer start = (i - setPageSize); start < i; ++start) {
                ListRequested.add(InnerList[start]);
            }
        }

        System.debug(ListRequested);
        return ListRequested;
    }

}