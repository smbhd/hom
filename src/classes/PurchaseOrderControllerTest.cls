/**
 * Created by HabibKhan on 5/9/2017.
 */

@IsTest
private class PurchaseOrderControllerTest {


    //test success
    static testMethod void testCreateNewPurchaseOrder() {
        POByBranchNumberWrapper.PurchaseOrderWrapper pow = new POByBranchNumberWrapper.PurchaseOrderWrapper();

        //RecordType BranchRecType = [SELECT id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Branch'];


        ///I need a branch contact
        Contact newBranContact = new Contact();
        newBranContact.LastName = 'BranchContact';
        newBranContact.Email = 'testBranchContact@test.com';
        insert newBranContact;

        //I need a branch account

        Account branchAccount = new Account();
        branchAccount.Branch_Number__c = '001';
        branchAccount.Branch_Contact__c = newBranContact.Id;

        Account newAccount = new Account();
        newAccount.Name = 'Test Alot';
        newAccount.BillingStreet = 'Ravenswood';
        newAccount.BillingCity = 'Chicago';
        newAccount.BillingState = 'Illinois';
        newAccount.BillingPostalCode = '60618';
        newAccount.Branch_Number__c = '001';
        newAccount.Branch_Contact__r = newBranContact;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        insert sku1;


        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5,  SKU__c=sku1.id);


        POByBranchNumberWrapper.LineItemWrapper liw = new POByBranchNumberWrapper.LineItemWrapper();
        liw.purchaseOrderLine = newLI1;

        pow.purchaseOrderLines = new List<POByBranchNumberWrapper.LineItemWrapper>();
        pow.purchaseOrderLines.add(liw);
        pow.purchaseOrder = new Purchase_Order__c();


        Contact anotherContact = new Contact();

        anotherContact.LastName = 'anotherContact';
        anotherContact.Email = 'anothercontact@test.com';

        pow.contact = anotherContact;
        pow.account = newAccount;
        pow.branch = branchAccount;

        String purchaseOrderJSON = JSON.serialize(pow);

        PurchaseOrderController.createNewProject(purchaseOrderJSON);

        List<CKSW_BASE__Service_Order__c> projects = [SELECT Id FROM CKSW_BASE__Service_Order__c];

        System.assertEquals(1,projects.size());

    }

    static testMethod void testGetSKUPrices(){

        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();



        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');

        skus.add(sku1);
        skus.add(sku2);
        insert skus;


        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id, SKU_Price_ID__c='1');
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id,SKU_Price_ID__c='1');


        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

                Map<String, List<SKU_Price__c>> skuMap = PurchaseOrderController.getSkuPrices();

        System.assertEquals(2,skuMap.get('1').size());
    }
    
    static testMethod void testGetPurchaseOrderNumbersFromSF(){
        RecordType standardRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'CKSW_Base__Service_Order__c' AND DeveloperName='Standard_Project'];
        CKSW_BASE__Service_Order__c newPayoutServiceOrder = new CKSW_BASE__Service_Order__c();
        newPayoutServiceOrder.CKSW_BASE__Status__c = 'Opened';
        newPayoutServiceOrder.RecordTypeId = standardRecordType.id;
        insert newPayoutServiceOrder;



        Purchase_Order__c po = new Purchase_Order__c();
        po.Purchase_Order_Number__c = '1';
        po.Project__c = newPayoutServiceOrder.Id;
        insert po;

        PurchaseOrderController.po newPo = new PurchaseOrderController.po();
        newPo.poNumber = '1';

        List<PurchaseOrderController.po> pos = new List<PurchaseOrderController.Po>();
        pos.add(newPo);

        Set<String> orderNums = PurchaseOrderController.getPurchaseOrderNumbersFromSF(pos);

        System.assertEquals(1, orderNums.size());

    }

    static testMethod void testSKUPriceCheck(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();



        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');

        skus.add(sku1);
        skus.add(sku2);
        insert skus;


        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id, SKU_Price_ID__c='1');
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id,SKU_Price_ID__c='1');


        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;
        List<SKU_Price__c> pricesUpdated = [SELECT Id,Unit_Price__c,Type__c,Service_Type__c FROM SKU_Price__c];




        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5,  SKU__c=sku1.id);

        POByBranchNumberWrapper.LineItemWrapper liw = new POByBranchNumberWrapper.LineItemWrapper();
        liw.purchaseOrderLine = newLI1;


        PurchaseOrderController.skuPriceCheck(liw,pricesUpdated);


        System.assertEquals(false,liw.lineItemHasError);
    }

    static testMethod void testGetStreetAddress(){


        Test.setMock(HttpCalloutMock.class, new AddressHttpCalloutMock());
        //valid address as far as I can tell
        String street = '4320 204TH ST';
        String city = 'BAYSIDE';
        String state = 'NY';
        String postal = '11361-2629';

        List<PurchaseOrderController.AddressResult> result = PurchaseOrderController.getAddressList(street,city,state,postal);

        System.assertNotEquals(0, result.size());

    }

}