/**
 * Created by AlexSanborn on 5/10/2017.
 */

@IsTest
public with sharing class HomeDepotTest {
    private static final String APIURL = 'http://www.dummy.com/authurl';
    private static final String CLIENTID = 'testId';
    private static final String CLIENTSECRET = 'testSecret';
    private static final String STATE = 'mockState';


    @isTest
    public static void testGetMetadataTypeMethod(){
        HomeDepot homeDepotCls = new HomeDepot();

        String actualResult = homeDepotCls.getCustomMetadataType();
        System.assertEquals(actualResult, 'HomeDepotAPI__mdt');
    }

    @isTest
    public static void testIntiateMethod(){
        String stateToPropogate = 'mockTestState';

        Map<String, String> authProviderConfiguration = new Map<String, String>();
        authProviderConfiguration.put('api_url__c', APIURL);
        authProviderConfiguration.put('client_id__c', CLIENTID);
        authProviderConfiguration.put('client_secret__c', CLIENTSECRET);

        HomeDepot homeDepotCls = new HomeDepot();
        PageReference expectedUrl = new PageReference('https://cs51.salesforce.com/apex/HomeDepotInitiate?state='+stateToPropogate);
        PageReference actualUrl = homeDepotCls.initiate(authProviderConfiguration, stateToPropogate);

        System.assertEquals(expectedUrl.getUrl(), actualUrl.getUrl());
    }

    @isTest
    public static void testHandleCallBackMethod(){
        Map<String, String> authProviderConfiguration = new Map<String, String>();
        authProviderConfiguration.put('api_url__c', APIURL);
        authProviderConfiguration.put('client_id__c', CLIENTID);
        authProviderConfiguration.put('client_secret__c', CLIENTSECRET);
        HomeDepot homeDepotCls = new HomeDepot();

        Test.setMock(HttpCalloutMock.class, new HomeDepotHttpCalloutMock());

        Map<String, String> queryParams = new Map<String, String>();
        queryParams.put('state', STATE);

        Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams);

        Auth.AuthProviderTokenResponse actualResponse = homeDepotCls.handleCallback(authProviderConfiguration, cbState);
        String token = homeDepotCls.getToken(authProviderConfiguration);

        Auth.AuthProviderTokenResponse expectedResponse = new Auth.AuthProviderTokenResponse('HomeDepot', token, 'refreshToken', STATE);

        System.assertEquals(actualResponse.oauthSecretOrRefreshToken, expectedResponse.oauthSecretOrRefreshToken);
        System.assertEquals(actualResponse.oauthToken, expectedResponse.oauthToken);
        System.assertEquals(actualResponse.provider, expectedResponse.provider);
        System.assertEquals(actualResponse.state, expectedResponse.state);
    }

    @isTest
    public static void testRefreshMethod(){
        Map<String, String> authProviderConfiguration = new Map<String, String>();
        authProviderConfiguration.put('api_url__c', APIURL);
        authProviderConfiguration.put('client_id__c', CLIENTID);
        authProviderConfiguration.put('client_secret__c', CLIENTSECRET);
        HomeDepot homeDepotCls = new HomeDepot();

        Test.setMock(HttpCalloutMock.class, new HomeDepotHttpCalloutMock());

        Auth.OAuthRefreshResult actualResult = homeDepotCls.refresh(authProviderConfiguration, null);

        String token = homeDepotCls.getToken(authProviderConfiguration);
        Auth.OAuthRefreshResult expectedResult = new Auth.OAuthRefreshResult(token, 'refreshToken');

        System.assertEquals(actualResult.accessToken, expectedResult.accessToken);
        System.assertEquals(actualResult.error, expectedResult.error);
        System.assertEquals(actualResult.refreshToken, expectedResult.refreshToken);
    }

    @isTest
    public static void testGetUserInfoMethod(){
        Map<String, String> authProviderConfiguration = new Map<String, String>();
        authProviderConfiguration.put('api_url__c', APIURL);
        authProviderConfiguration.put('client_id__c', CLIENTID);
        authProviderConfiguration.put('client_secret__c', CLIENTSECRET);
        HomeDepot homeDepotCls = new HomeDepot();

        Test.setMock(HttpCalloutMock.class, new HomeDepotHttpCalloutMock());

        Map<String, String> queryParams = new Map<String, String>();
        queryParams.put('state', STATE);
        Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams);
        Auth.AuthProviderTokenResponse response = homeDepotCls.handleCallback(authProviderConfiguration, cbState);
        Auth.UserData actualResult = homeDepotCls.getUserInfo(authProviderConfiguration, response);

        Auth.UserData expectedResult = new Auth.UserData(null, null, null, null, null, null, null, null, 'HomeDepot', null, null);

        System.assertEquals(actualResult.provider, expectedResult.provider);
    }
}