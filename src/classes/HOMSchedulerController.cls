/**
 * Created by HabibKhan on 5/10/2017.
 */


public class HOMSchedulerController {
    public String workOrderId { get; set; }
    public String errorString {get;set;}

    public HOMSchedulerController(){

        errorString = null;
        String id = ApexPages.currentPage().getParameters().get('wo');

        if(id != null){
            try {

                workOrderId = id;
                CKSW_BASE__Calendar__c calendar1 = [SELECT Id,CKSW_BASE__Exact_Appointments__c, (SELECT CKSW_BASE__Finish_Time__c, CKSW_BASE__Start_Time__c, CKSW_BASE__Type__c, RecordType.DeveloperName FROM CKSW_BASE__Days__r) FROM CKSW_BASE__Calendar__c where Name = 'Base Calendar'];
                CKSW_BASE__Scheduling_Policy__c scheduling_policy = [SELECT Id from CKSW_BASE__Scheduling_Policy__c WHERE Name = 'Standard Policy'];


                 CKSW_SRVC.ScheduleResult result = CKSW_SRVC.ScheduleService.Schedule(scheduling_policy.Id, id);

                // System.debug(result);


                List<CKSW_SRVC.AppointmentBookingSlot> appointSlots = CKSW_SRVC.AppointmentBookingService.GetSlots(id, scheduling_policy.Id, calendar1, UserInfo.getTimeZone(), CKSW_SRVC.AppointmentBookingService.SortResultsBy.NOSORT);


                System.debug(appointSlots);


                //grab project id from work order

                //From work order get location and service skills then
                CKSW_BASE__Service__c wo = [
                        SELECT Id, CKSW_BASE__Location__c, Contractor_Company__c, (SELECT Id, CKSW_BASE__Skill__c FROM CKSW_BASE__Service_Skills__r)
                        FROM CKSW_BASE__Service__c
                        WHERE Id = :id
                ];
                // from location get contractor companies locations
                CKSW_BASE__Location__c location = [SELECT Id, (SELECT Id,Contractor_Company__c FROM Contractor_Company_Locations__r) FROM CKSW_BASE__Location__c WHERE id = :wo.CKSW_BASE__Location__c];
                List<Id> contract_companyIds = new List<Id>();

                for (Contractor_Company_Location__c ccl: location.Contractor_Company_Locations__r) {
                    contract_companyIds.add(ccl.Contractor_Company__c);
                }

                //from which we can get contract companies
                List<Contractor_Company__c> contractor_companies = [
                        SELECT Id, (SELECT Id FROM Technicians__r), (SELECT Id FROM Skills__r)
                        FROM Contractor_Company__c
                        WHERE Id in:contract_companyIds
                ];

                List<Id> resourceIds = new List<Id>();


                //filter contract companies by skills
                List<Contractor_Company__c> filteredCompanies = new List<Contractor_Company__c>();
                for (Contractor_Company__c cc: contractor_companies) {

                    //for each of the contract companies I build a map, and check to see if it contains all the skills required by
                    // the wo. If it does not contain any one of them, then I will not add it to my list
                    if (cc.Skills__r != null) {
                        Map<Id, CKSW_BASE__Skill__c> ccSkills = new Map<Id, CKSW_BASE__Skill__c>(cc.Skills__r);

                        Boolean remove = false;
                        for (CKSW_BASE__Service_Skill__c ss: wo.CKSW_BASE__Service_Skills__r) {
                            if (!ccSkills.containsKey(ss.CKSW_BASE__Skill__c)) {
                                remove = true;
                                //it doest contain a particular skill therefore we wont add it to out filtered list;
                                break;
                            }
                        }
                        if (!remove) {
                            filteredCompanies.add(cc);
                        }
                    }
                }


                for (Contractor_Company__c c : contractor_companies) {
                    for (CKSW_BASE__Resource__c resource: c.Technicians__r) {
                        resourceIds.add(resource.Id);
                    }
                }

                // from which we can get resources
                List<CKSW_BASE__Resource__c> resources = [
                        SELECT Id, (SELECT Id, CKSW_BASE__Calendar__c FROM CKSW_BASE__Resource_Calendars__r)
                        FROM CKSW_BASE__Resource__c
                        WHERE Id in:resourceIds
                ];

                // which will have a calendar that will have working hours

                List<Id> calendarIds = new List<Id>();


                for (CKSW_BASE__Resource__c resource: resources) {
                    for (CKSW_BASE__Resource_Calendar__c calendar : resource.CKSW_BASE__Resource_Calendars__r) {
                        calendarIds.add(calendar.CKSW_BASE__Calendar__c);
                    }
                }


                List<CKSW_BASE__Calendar__c> calendars = [SELECT Id, (SELECT Id, CKSW_BASE__Type__c,CKSW_BASE__Start_Time__c,CKSW_BASE__Finish_Time__c FROM CKSW_BASE__Days__r) FROM CKSW_BASE__Calendar__c];


                // With the working hours we can build the working hours


                //grab contractor ompany

                //grab resources from contractor company uses location matches the work order's location


            }catch (Exception e){
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                errorString = 'Something went wrong. Please try again later!';

            }
            //grab Work Order Id from URL

            //grab dates from Salesforce that will be blocked off
        }
    }



    //Once we figure out where we are getting data from I can update this
    @RemoteAction
    public static String getAppointments(){
        List<HOMSchedulerController.AppointmentWrapper> appts = new List<HOMSchedulerController.AppointmentWrapper>();

        for(Integer i = 0; i < 7; i++) {
            HOMSchedulerController.AppointmentWrapper a = new HOMSchedulerController.AppointmentWrapper();
            a.title = 'Test '+i;
            a.start = System.now().addDays(i).addHours(i+4);
            a.endtime = System.now().addDays(i).addHours(i+7);
            appts.add(a);
        }

        System.debug(JSON.serializePretty(appts));
        return JSON.serializePretty(appts);
    }

    @RemoteAction
    public static String makeAppointment(String appt){

        HOMSchedulerController.AppointmentWrapper apw = (HOMSchedulerController.AppointmentWrapper)JSON.deserialize(appt,HOMSchedulerController.AppointmentWrapper.class);

        System.debug(apw);
        Datetime start = Datetime.valueOf(apw.start+'');
        Datetime endtime = Datetime.valueOf(apw.endtime+'');

        CKSW_BASE__Service__c workOrder = new CKSW_BASE__Service__c();

        workOrder.Id = apw.id;
        workOrder.CKSW_BASE__Appointment_Start__c = start;
        workOrder.CKSW_BASE__Appointment_Finish__c = endtime;
        workOrder.Self_Service_Schedule_Status__c = 'PendingConfirm';

        //todo if resource is a seperate object we might need to remove it here
        try{
            update workOrder;
        }catch (Exception e){
            System.debug('Error is '+ e.getMessage());
            return 'Error making appointment ' +e.getMessage();
        }

        return 'Success';
    }

    @RemoteAction
    public static String insertNote(String noteJSON){

        NoteWrapper newNote = (NoteWrapper)JSON.deserialize(noteJSON,NoteWrapper.class);
        System.debug(newNote);

        CKSW_BASE__Service__c wo = new CKSW_BASE__Service__c();
        wo.Customer_Message__c =  newNote.Title +'-----' + newNote.Body;
        wo.Latest_Customer_Message__c = System.now();
        wo.Id = newNote.ParentId;
        wo.Self_Service_Schedule_Status__c ='CustomerCancelled';

        try{
             update wo;
        }catch (Exception e){
            System.debug(e.getMessage());
            return e.getMessage();
        }
        return 'Success';
    }


    public class AppointmentWrapper{
        public String title;
        public Datetime start;
        public Datetime endtime;
        public String id;
    }

    public class NoteWrapper{
        public String Title;
        public String Body;
        public String ParentId;
    }

}