/**
 * Created by AlexSanborn on 5/3/2017.
 */

public with sharing class PurchaseOrderService {
    public static void deleteProjectsWithoutPO(List<Purchase_Order__c> purchaseOrders){
        List<id> projectIds = new List<id>();
        List<CKSW_BASE__Service_Order__c> projectsWithoutPurchaseOrders = new List<CKSW_BASE__Service_Order__c>();

        for(Purchase_Order__c po : purchaseOrders){
            projectIds.add(po.Project__c);
        }

        for (CKSW_BASE__Service_Order__c project : [SELECT id, (SELECT Id FROM Purchase_Orders__r) FROM CKSW_BASE__Service_Order__c WHERE id in :projectIds]) {
            if (project.Purchase_Orders__r.size() == 0) {
                projectsWithoutPurchaseOrders.add(project);
            }
        }
        delete projectsWithoutPurchaseOrders;

    }

}