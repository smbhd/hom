/**
 * Created by HabibKhan on 5/9/2017.
 */

@IsTest
private class PurchaseOrderTriggerTest {

    static testMethod void shouldDeleteOneProject() {

        RecordType standardRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'CKSW_Base__Service_Order__c' AND DeveloperName='Standard_Project'];
        CKSW_BASE__Service_Order__c newPayoutServiceOrder = new CKSW_BASE__Service_Order__c();
        newPayoutServiceOrder.CKSW_BASE__Status__c = 'Opened';
        newPayoutServiceOrder.RecordTypeId = standardRecordType.id;
        insert newPayoutServiceOrder;

        CKSW_BASE__Service_Order__c newPayoutServiceOrder1 = new CKSW_BASE__Service_Order__c();
        newPayoutServiceOrder1.CKSW_BASE__Status__c = 'Opened';
        newPayoutServiceOrder1.RecordTypeId = standardRecordType.id;
        insert newPayoutServiceOrder1;

        Purchase_Order__c newPOPayout = new Purchase_Order__c(Project__c=newPayoutServiceOrder.id);

        insert newPOPayout;

        newPOPayout.Project__c = newPayoutServiceOrder1.Id;
        update newPOPayout;

        List<CKSW_BASE__Service_Order__c> projects = [SELECT Id,Name FROM CKSW_BASE__Service_Order__c];

        System.assertEquals(1, projects.size());
    }
}