/**
 * Created by HabibKhan on 5/23/2017.
 */

@IsTest
private class MeasurementsControllerTest {
    static testMethod void createPOAndMeasure() {
        RecordType standardRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'CKSW_Base__Service_Order__c' AND DeveloperName='Standard_Project'];

        CKSW_BASE__Service_Order__c newPayoutServiceOrder = new CKSW_BASE__Service_Order__c();
        newPayoutServiceOrder.CKSW_BASE__Status__c = 'Opened';
        newPayoutServiceOrder.RecordTypeId = standardRecordType.id;
        insert newPayoutServiceOrder;

        Purchase_Order__c newPOPayout = new Purchase_Order__c(Project__c=newPayoutServiceOrder.id);
        insert newPOPayout;

        Measure__c m = new Measure__c();
        m.Purchase_Order__c = newPOPayout.Id;
        insert m;

        List<Measure__c> measures = MeasurementsController.getMeasurement(newPOPayout.id);

        System.assertNotEquals(measures,null);

    }
}