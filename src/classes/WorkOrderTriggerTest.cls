/**
 * Created by AlexSanborn on 4/6/2017.
 */

@IsTest(SeeAllData=true)
public class WorkOrderTriggerTest {

    @isTest
    public static void createsNewPayout(){
        //start setup

        List<CKSW_BASE__Service__c> newServices = new List<CKSW_BASE__Service__c>();
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Purchase_Order_Line__c> lineItems = new List<Purchase_Order_Line__c>();
        List<Payout_Price__c> payoutPrices = new List<Payout_Price__c>();
        List<CKSW_BASE__Service_Product__c> workOrderLineItemList = new List<CKSW_BASE__Service_Product__c>();



        RecordType standardRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'CKSW_Base__Service_Order__c' AND DeveloperName='Standard_Project'];
        CKSW_BASE__Service_Order__c newPayoutServiceOrder = new CKSW_BASE__Service_Order__c();
        newPayoutServiceOrder.CKSW_BASE__Status__c = 'Opened';
        newPayoutServiceOrder.RecordTypeId = standardRecordType.id;
        insert newPayoutServiceOrder;

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;

        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, Service_Type__c=sku1.id, Unit_Price__c=4.5, SKU__c=skuProduct1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, Service_Type__c=sku2.id, Unit_Price__c=1.2, SKU__C=skuProduct2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;



        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newPayoutServiceOrder.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.CKSW_BASE__Early_Start__c = datetime.now();
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = skus.get(0).id;
        newService1.CKSW_BASE__Duration__c = 6;
        newService1.CKSW_BASE__Duration_Type__c = 'Hours';
        newService1.CKSW_BASE__Street__c = '4147 Ravenswood';
        newService1.CKSW_BASE__City__c = 'Chicago';
        newService1.CKSW_BASE__State__c = 'IL';
        newService1.CKSW_BASE__Zip__c = '60613';
        insert newService1;


        CKSW_BASE__Scheduling_Policy__c policy = [SELECT id FROM CKSW_BASE__Scheduling_Policy__c LIMIT 1];
        CKSW_SRVC.ScheduleResult result = CKSW_SRVC.ScheduleService.Schedule(policy.id, newService1.id);

        CKSW_BASE__Resource__c workCrew = [SELECT id FROM CKSW_BASE__Resource__c LIMIT 1];

        CKSW_BASE__Required_Resource__c newRequiredResource = new CKSW_BASE__Required_Resource__c(CKSW_BASE__Service__c=newService1.id, CKSW_BASE__Resource__c=workCrew.id);
        insert newRequiredResource;


        newService1.CKSW_BASE__Appointment_Start__c = datetime.now();
        newService1.CKSW_BASE__Appointment_Finish__c = datetime.now().addHours(1);
        update newService1;


        Purchase_Order__c newPOPayout = new Purchase_Order__c(Project__c=newPayoutServiceOrder.id);
        insert newPOPayout;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOPayout.id, SKU__c=sku1.id);
        Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOPayout.id, SKU__c=sku2.id);
        lineItems.add(newLI1);
        lineItems.add(newLI2);
        insert lineItems;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(CKSW_BASE__Quantity_Used__c=5, Service_Units__c='Square Foot', CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id, Purchase_Order_Line__c=newLI1.id, Service_Unit_Price__c=4.00);
        CKSW_BASE__Service_Product__c newWOLI2 = new CKSW_BASE__Service_Product__c(CKSW_BASE__Quantity_Used__c=5, Service_Units__c='Square Foot',
                SKU__c=sku2.id, Purchase_Order_Line__c=newLI2.id, Service_Unit_Price__c=3.00, CKSW_BASE__Service__c=newService1.id);
        workOrderLineItemList.add(newWOLI1);
        workOrderLineItemList.add(newWOLI2);
        insert workOrderLineItemList;

        Account generalContractor = new Account(Name='General Contractor');
        insert generalContractor;

        Servicing_Location__c SL = [SELECT id FROM Servicing_Location__c LIMIT 1];

        Payout_Price__c newPayoutPrice1 = new Payout_Price__c(Name='flooring', General_Contractor__c=generalContractor.id, SKU__c=sku1.id, Unit_Price__c=4.4);
        Payout_Price__c newPayoutPrice2 = new Payout_Price__c(Name='flooring', isGlobal__c=TRUE, SKU__c=sku1.id, Unit_Price__c=4.5);
        Payout_Price__c newPayoutPrice3 = new Payout_Price__c(Name='flooring', Servicing_Location__c = SL.id, SKU__c=sku1.id, Unit_Price__c=4.7);

        payoutPrices.add(newPayoutPrice2);
        payoutPrices.add(newPayoutPrice3);
        payoutPrices.add(newPayoutPrice1);
        insert payoutPrices;

        //end setup

        Integer payoutCount= [SELECT Count() FROM PayoutChargeback__c WHERE Project__c=:newPayoutServiceOrder.id];
        System.assertEquals(0, payoutCount);

        newservice1.CKSW_BASE__Status__c = 'Work Complete';
        update newservice1;

//        CKSW_BASE__Service__c serviceUpdated = [SELECT id, CKSW_BASE__Status__c FROM CKSW_BASE__Service__c WHERE CKSW_BASE__Service_Order__c=:newPayoutServiceOrder.id LIMIT 1];

        System.assertEquals('Work Complete', newService1.CKSW_BASE__Status__c);

        Integer payoutCountUpdated= [SELECT Count() FROM PayoutChargeback__c WHERE Project__c=:newPayoutServiceOrder.id];
        System.assertEquals(2, payoutCountUpdated);
    }
}