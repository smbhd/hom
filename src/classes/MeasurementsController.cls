public class MeasurementsController {


    public static final String USERID = 'st22902';
    public static final String PASSWORD = '22902';
    public static final List<String> HDFIELDS = new List<String>{
            'CalcTimeStamp', 'LineItemNumber', 'MaterialType', 'MaterialDescription', 'MaterialWidth', 'PatternMatch',
            'MaterialAmount', 'MeasureNumber', 'CrossStreetDir1',
            'CrossStreet1', 'CrossstreetDir2', 'CrossStreet2',
            'Heat', 'Pets', 'Electricity', 'FurnitureMoving',
            'NewPaint', 'Removal', 'PavedDriveway', 'Garage',
            'Elevator', 'Disposal', 'Access', 'SiteType'
    };


    public static final List<String> SFFIELDS = new List<String>{
            'Calculation_Time__c', 'Line_Item__c', 'Material_Type__c', 'Material_Description__c', 'Material_Width__c',
            'Pattern_Match__c', 'Material_Amount__c', 'HD_Measure_Number__c',
            'Cross_Street_1_Direction__c', 'Cross_Street_1__c',
            'Cross_Street_2_Direction__c', 'Cross_Street_2__c',
            'Heat__c', 'Pets__c', 'Electricity__c', 'Furniture_Moving__c',
            'New_Paint__c', 'Removal__c', 'Paved_Driveway__c', 'Garage__c',
            'Elevator__c', 'Disposal__c', 'Access__c', 'Site_Type__c'
    };


    @AuraEnabled
    public static String validatePo(String id){
        Purchase_Order__c po = MeasurementsController.getPurchase_order(id);

        String error = 'You need to fill out the following fields: ';
        Boolean noError = true;
        if(po.Purchase_Order_Number__c == null){
            error+= ' Purchase Order Number on Purchase Order ';
            noError = false;
        }
        if(po.InstallerOrderNumber__c == null){
            error+= ' Installer Number on Purchase Order ';
            noError = false;
        }

        if(po.Project__r.Branch__r.Branch_Number__c == null){
            error+= ' Branch number for the branch associated with the project ';
            noError = false;
        }

        if(po.Project__r.CKSW_BASE__Account__r.Phone == null){
            error+= ' Phone number for account associated with the project ';
            noError = false;
        }

        if(noError){
            error = null;
        }
        System.debug(error);
        return error;
    }


    @AuraEnabled
    public static List<Measure__c> getMeasurement(String poId) {

        //grab all fields from Measure
        Set<String> measurementFields = Schema.getGlobalDescribe().get('Measure__c').getDescribe().fields.getMap().keySet();
        System.debug('SELECT ' + String.join(new List<String>(measurementFields), ',') + ' FROM Measure__c WHERE Purchase_Order__c = \'' + poId + '\'');
        List<Measure__c> measures = Database.query('SELECT ' + String.join(new List<String>(measurementFields), ',') + ' FROM Measure__c WHERE Purchase_Order__c = \'' + poId + '\'');

        if (measures.size() != 0) {
            return measures;
        }

        return null;
    }


    @AuraEnabled
    public static String getMeasurementsFromCallout(String Id) {
        Boolean debug = false;

        try {


            Purchase_Order__c po = MeasurementsController.getPurchase_order(Id);
            System.debug(po);


            //todo installordernumber = new field JT just made which will be on the PO

            String endpoint;
            if (debug) {
                endpoint = 'https://www.measurecomp.com/scripts/installercomm.exe/GetJobData?UserID=st22902&Password=22902&StoreNumber=1515&InstallPO=15474447&InstallOrderNumber=423355&PhoneNumber=9703978959';
            } else {
                String phonenumber = po.Project__r.CKSW_BASE__Account__r.Phone;
                phonenumber = phonenumber.remove('(');
                phonenumber = phonenumber.remove(')');
                phonenumber = phonenumber.remove('-');
                phonenumber = phonenumber.remove(' ');

                endpoint = 'https://www.measurecomp.com/scripts/installercomm.exe/GetJobData?'+'' +
                        'UserId=' + MeasurementsController.userid +
                        '&Password=' + MeasurementsController.password +
                        '&StoreNumber=' + po.Project__r.Branch__r.Branch_Number__c
                        + '&InstallPO=' + po.Purchase_Order_Number__c +
                        '&InstallOrderNumber=' + po.InstallerOrderNumber__c +
                        '&PhoneNumber=' + phonenumber;
            }


            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);

            request.setMethod('GET');
            Http http = new Http();
            HttpResponse response = http.send(request);


            System.debug(response.getBody());

            List<Measure__c> result = MeasurementsController.parseXMLToList(response.getBody());


            System.debug(result);
            if (result.size() != 0) {
                return JSON.serialize(result);
            }

        } catch (Exception e) {
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());


        }
        return null;
    }

    private static List<Measure__c> parseXMLToList(String xml) {


        Dom.Document doc = new Dom.Document();
        doc.load(xml);
        System.debug(doc);
        List<Measure__c> results = new List<Measure__c>();
        List<Measure__c> measures;
        for (Dom.XmlNode node: doc.getRootElement().getChildElements()) {
            System.debug(node);

            //this will pass in <InstallerJob> tag
            measures = MeasurementsController.processNode(node);
            System.debug(measures);
            if (measures != null) {
                results.addAll(measures);
            }
            //null out just for sake of completeness
            measures = null;
        }

        return results;
    }

    private static List<Measure__c> processNode(Dom.XmlNode node) {

        if (node.getName() == 'InstallerJob') {
            //grab attribute values off of installerjob tag
            Map<String, String> fieldMappings = new Map<String, String>();

            if (HDFIELDS.size() != SFFIELDS.size()) {
                throw new MeasureException('Field Mappings are messed up!!');
                return null;
            }

            System.debug(node);

            //this is a mapping from HD fields to salesforce fields
            //
            for (Integer i = 0; i != HDFIELDS.size(); i++) {
                fieldMappings.put(HDFIELDS.get(i), SFFIELDS.get(i));
            }
            System.debug(fieldMappings);


            List<Measure__c> measures = new List<Measure__c>();

            Measure__c measure = new Measure__c();
            //this will grab all the necessary fields off of the InstallerJob Tag using the field mappings
            MeasurementsController.copyOverFields(fieldMappings, node, measure);
            //We will be looking at inner calc nodes now
            for (Dom.XmlNode calcNode: node.getChildElements()) {

                Measure__c deepCopy = measure.clone(false,true);

                System.debug(calcNode);

                //We only care about calc tag with assigned = true
                String namespace = calcNode.getNamespace();
                System.debug('Copying over calc fields');

                //This will grab all the necessary fields off calc tag and add them to the measure object
                MeasurementsController.copyOverFields(fieldMappings, calcNode, deepCopy);

                //We get the line item list entry and we grab the fields off of that
                for(Dom.XmlNode lileNode: calcNode.getChildElements()){
                    System.debug('Copying over lile fields');
                    Measure__c deepCopyOfCalc = deepCopy.clone(false,true);
                    MeasurementsController.copyOverFields(fieldMappings, lileNode, deepCopyOfCalc);
                    measures.add(deepCopyOfCalc);
                }

            }
            return measures;
        }
        return null;
    }

    //this helper function copies the attribute values off of the Node and onto the measure object or the line items
    private static void copyOverFields(Map<String, String>fieldMappings, Dom.XmlNode node, Measure__c measure) {
        for (Integer i = 0; i != node.getAttributeCount(); i++) {
            String namespace = node.getNamespace();
            String attrKey = node.getAttributeKeyAt(i);
            String sfField = fieldMappings.get(attrKey);
            if (node.getAttributeValue(attrKey, namespace) != null && sfField != null) {
                if(sfField == 'Calculation_Time__c'){
                    //todo figure this out
                    String times = node.getAttributeValue(attrKey, namespace);
                    //the time is in format HH:mm:ss and salesforce needs hh:mm A
                    String Datestr = times.substring(0,10);
                    String hour = times.substring(11,13);
                    System.debug(hour);
                    String minute = times.substring(14,16);
                    System.debug(minute);

                    Integer hours = Integer.valueOf(hour);
                    String amOrPm ='';

                    if(hours <= 12){
                        amOrPm= 'AM';
                    }else{
                        amOrPm = 'PM';
                    }

                    hours = Math.mod(hours,  12);
                    hours = hours == 0 ? 12 : hours;
                    String parsedTime = Datestr +' ' + hours+ ':' +minute+' '+amOrPm;
                    System.debug(parsedTime);
                    Datetime calcTime = Datetime.parse(parsedTime);
                    measure.put('Calculation_Time__c', calcTime);
                }else{
                    measure.put(sfField, node.getAttributeValue(attrKey, namespace));
                }
            } else {
                System.debug('Node val is ' + node.getAttributeValue(attrKey, namespace));
                System.debug('Salesforce field is ' + sfField);
            }
        }
    }


    @AuraEnabled
    public static String upsertMeasure(String poId, String measureJSON){
        try{
            //Grab the details from the purchase order
            Purchase_Order__c po = MeasurementsController.getPurchase_order(poId);

            //One requirement is that we only want one measure/purchase order so we delete the ones that are there
           // List<Measure__c> measuresToDelete = [SELECT Id FROM Measure__c WHERE Purchase_Order__c =: poId];
            //On meeting with client 5/23/2017 they would like to have more than one measure under a purchase order

            Measure__c m = (Measure__c)JSON.deserialize(measureJSON,Measure__c.class);
            System.debug(m);
            m.Purchase_Order__c = Id.valueOf(poId);

            //Get Plan.pdf from 2nd callout
            Blob responseBody = MeasurementsController.measurePDFCallout(po,m);

            insert m;


            //reason for doing this here
            if(responseBody != null){
                Attachment a = new Attachment();
                a.parentId = m.Id;
                a.Name = 'Plan.pdf';
                a.ContentType = 'application/pdf';
                a.Body = responseBody;
                insert a;
            }

            return JSON.serialize(m);
        }catch (Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        }

        return null;
    }


    private static Purchase_Order__c getPurchase_order(String Id){
        Purchase_Order__c po = [
                SELECT Id, Name, Project__r.CKSW_BASE__Account__r.Phone,
                        Project__r.Vendor_Order_Number__c, Project__r.Branch__r.Branch_Number__c
                        ,Purchase_Order_Number__c,InstallerOrderNumber__c
                FROM Purchase_Order__c
                WHERE ID = :Id
        ];
        return po;
    }


    private static Blob measurePDFCallout(Purchase_Order__c po, Measure__c measure){

        Boolean debug = false;
        try{
            String endpoint ='';
            if(debug){
                endpoint= 'https://www.measurecomp.com/scripts/renderpdfsys.exe/InstallerGetPlan?UserID=st22902&Password=22902&StoreNumber=3502&InstallPO=02402317&InstallOrderNumber=589520&MeasureNumber=12665661&CalcDateTime=05162017183948';
            }else{
                endpoint='https://www.measurecomp.com/scripts/renderpdfsys.exe/InstallerGetPlan?UserId=' + MeasurementsController.userid +
                        '&Password=' + MeasurementsController.password+
                        '&StoreNumber=' + po.Project__r.Branch__r.Branch_Number__c
                        + '&InstallPO=' + po.Purchase_Order_Number__c +
                        '&InstallOrderNumber=' + po.InstallerOrderNumber__c +
                        '&MeasureNumber=' + measure.HD_Measure_Number__c+
                        '&CalcDateTime=' +measure.Calculation_Time__c.format('MMddYYYYHHmmss');
            }

            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);

            request.setMethod('GET');
            Http http = new Http();
            HttpResponse response = http.send(request);


            System.debug(response.getBodyAsBlob());



            return response.getBodyAsBlob();

        }catch (Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        }

        return null;
    }




    public class MeasureException extends Exception {
    }

}