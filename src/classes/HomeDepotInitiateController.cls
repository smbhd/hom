/**
 * Created by AsharSaad on 3/21/2017.
 */

public class HomeDepotInitiateController {
	public HomeDepotInitiateController() {

	}

	public PageReference callout() {
		String state = ApexPages.currentPage().getParameters().get('state');
		return new PageReference('https://cs51.salesforce.com/services/authcallback/00D4B000000CyEaUAK/HomeDepot?state='+state);
	}
}