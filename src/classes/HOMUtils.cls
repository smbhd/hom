public class HOMUtils {
	/**
	 * @param   endpoint  the endpoint on HD's api e.g. po/list
	 * @param   body      the json serialized body we need to send e.g. {"search": {"poStatus": "Measure Requested"}}
	 * @return            a CalloutResponse object of the http callout
	 */
	public static CalloutResponse calloutToHomeDepot(String endpoint, String body) {
		HttpRequest req = new HttpRequest();
		req.setEndpoint('callout:HomeDepotAPI/'+endpoint);
		req.setMethod('POST');
		req.setHeader('Content-Type', 'application/json');
		req.setHeader('appToken', '{!$Credential.OAuthToken}');
		req.setBody(body);
		req.setTimeout(120000);

		Http http = new Http();
		HttpResponse res = http.send(req);

		System.debug(req.getBody());
		System.debug(res.getBody());

		return new CalloutResponse(res.getBody(), res.getStatusCode());
	}

	public static String addressLookup(String street, String city, String state, String postalcode) {
		HttpRequest request = new HttpRequest();

		String apiKey = 'AIzaSyCJ-twGq6dtFSsebxH43ZpsWIcJM3-c0pI'; //******THIS IS A TEMP KEY, BE SURE TO REPLACE IT WITH HOM'S KEY
		String streetCallout = street.replaceAll(' ', '+');
		String cityCallout = city.replaceAll(' ', '+');
		String stateCallout = state.replaceAll(' ', '+');

		request.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address=' +
				EncodingUtil.urlEncode(streetCallout, 'UTF-8') + ',+' +
				EncodingUtil.urlEncode(cityCallout, 'UTF-8') + ',+' +
				EncodingUtil.urlEncode(stateCallout, 'UTF-8') +
				'&key=' + apiKey
		);

		request.setMethod('GET');

		Http http = new Http();
		HttpResponse response = http.send(request);

//		System.debug('For address: ' + street + ' ' + city + ', ' + state + ' ' + postalcode);
//		System.debug('Response: ' + response.getBody());

		return response.getBody();
	}
}

