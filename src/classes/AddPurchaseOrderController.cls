/**
 * Created by AlexSanborn on 5/1/2017.
 */

//fetch POs from Salesforce that have already been created for users to add them to Projects
public with sharing class AddPurchaseOrderController {
    @AuraEnabled
    public static List<poWithLineItems> getAllOpenPurchaseOrdersAndLineItems(Id projectId){
        List<poWithLineItems> lineItemsByPurchaseOrders = new List<poWithLineItems>();

        List<Purchase_Order__c> purchaseOrders = getAllOpenPurchaseOrders(projectId);
        List<id> poIds = new List<id>();

        for(Purchase_Order__c po : purchaseOrders){
            poIds.add(po.id);
        }

        List<Purchase_Order_Line__c> lineItems = getAllLineItems(poIds);

        for(Purchase_Order__c po : purchaseOrders){
            poWithLineItems newWrapper = new poWithLineItems();
            newWrapper.purchaseOrder = po;
            newWrapper.lineItems = new List<Purchase_Order_Line__c>();

            for(Purchase_Order_Line__c li : lineItems){
                if(li.Purchase_Order__c == po.id){
                    newWrapper.lineItems.add(li);
                }
            }
            lineItemsByPurchaseOrders.add(newWrapper);
        }

        System.debug(lineItemsByPurchaseOrders);
        return lineItemsByPurchaseOrders;
    }

    @AuraEnabled
    public static void addPurchaseOrder(Id projectId, Id purchaseOrderId){
        System.debug(projectId);
        System.debug(purchaseOrderId);
        Purchase_Order__c po = new Purchase_Order__c(id=purchaseOrderId, Project__c=projectId);
        update po;
        System.debug(po);

    }

    public static List<Purchase_Order__c> getAllOpenPurchaseOrders(Id projectId){
        CKSW_BASE__Service_Order__c project = [SELECT id, CKSW_BASE__Account__c FROM CKSW_BASE__Service_Order__c WHERE id=:projectId];

        List<CKSW_BASE__Service_Order__c> projectsWithAccount = [SELECT id FROM CKSW_BASE__Service_Order__c WHERE CKSW_BASE__Account__c =:project.CKSW_BASE__Account__c];

        List<id> projectIds = new List<id>();
        for(CKSW_BASE__Service_Order__c p : projectsWithAccount){
            projectIds.add(p.id);
        }
        return [SELECT id, Name FROM Purchase_Order__c
                                        WHERE Order_Status__c='Open' AND Project__c IN :projectIds AND Project__c!=:projectId];
    }

    public static List<Purchase_Order_Line__c> getAllLineItems(List<id> purchaseOrderIds){
        return[SELECT id, Item_Name__c, Purchase_Order__c, Quantity__c, Unit_of_Measure__c, Unit_Price__c FROM Purchase_Order_Line__c WHERE Purchase_Order__c IN :purchaseOrderIds];
    }

    public class poWithLineItems {
        @AuraEnabled
        public Purchase_Order__c purchaseOrder;
        @AuraEnabled
        public List<Purchase_Order_Line__c> lineItems;
    }
}