/**
 * Created by AlexSanborn on 4/11/2017.
 */

global class CreateChargebacks Implements Schedulable{
    public static String scheddailymidnight = '0 0 0 * * ?';

    global void execute(SchedulableContext context){
        List<PayoutChargeback__c> chargebacksToCreate = new List<PayoutChargeback__c>();

        List<CKSW_BASE__Service_Order__c> projectsWithChargebacks = [
                SELECT Id, Initial_Work_Order__c, Number_of_Installments__c, Interval__c, Initial_Project__c,
                        Next_Installment_Date__c, Chargeback_Status__c, Chargebacks_Complete__c
                FROM CKSW_BASE__Service_Order__c
                WHERE Next_Installment_Date__c=TODAY AND Chargebacks_Complete__c=FALSE];
        System.debug(projectsWithChargebacks);



        List<id> servicesWithChargebacks = new List<id>();
        for(CKSW_BASE__Service_Order__c so : projectsWithChargebacks){
            servicesWithChargebacks.add(so.Initial_Work_Order__c);
        }

        List<CKSW_BASE__Service__c> chargebackServices = [
                SELECT id, Contractor_Company2__c, CKSW_BASE__Service_Type__c, CKSW_BASE__Service_Order__c
                FROM CKSW_BASE__Service__c
                WHERE id IN :servicesWithChargebacks];

        System.debug(chargebackServices);


        List<CKSW_BASE__Service_Product__c> lineItems = [
                SELECT id, CKSW_BASE__Quantity_Used__c, Service_Units__c,
                        Service_Unit_Price__c, SKU__c, Purchase_Order_Line__r.Item_Name__c,
                        CKSW_BASE__Service__r.Contractor_Company2__c, CKSW_BASE__Service__c
                 FROM CKSW_BASE__Service_Product__c
                WHERE CKSW_BASE__Service__c IN :chargebackServices];
        System.debug(lineItems);

        List<id> skusFromLineItems = new List<id>();
        if(lineItems != null){
            for(Integer li=0; li<lineItems.size(); li++){
                if(lineItems.get(li).SKU__c != null){
                    skusFromLineItems.add(lineItems.get(li).SKU__c);
                }
            }
        }

        List<id> contractorCompanyIds = new List<id>();
        for(Integer cs=0; cs<chargebackServices.size(); cs++){
            contractorCompanyIds.add(chargebackServices.get(cs).Contractor_Company2__c);
        }

        List<Chargeback_Rate__c> chargebackRates = [SELECT id, Contractor_Company2__c, Rate__c, Rate_Type__c, SKU__c, Unit_of_Measure__c
        FROM Chargeback_Rate__c WHERE (SKU__c IN :skusFromLineItems) OR (Contractor_Company2__c IN :contractorCompanyIds)];

        Map<id, List<Chargeback_Rate__c>> skusByLineItems = new Map <id, List<Chargeback_Rate__c>>();

        if(lineItems != null){
            for(Integer li=0; li<lineItems.size(); li++){
                if((lineItems.get(li).SKU__c != null && !skusByLineItems.containsKey(lineItems.get(li).id))){
                    List<Chargeback_Rate__c> chargebackRatesList = new List<Chargeback_Rate__c>();
                    skusByLineItems.put(lineItems.get(li).id, chargebackRatesList);
                }

                if(skusByLineItems.containsKey(lineItems.get(li).id)){
                    Id currentSKU = lineItems.get(li).SKU__c;
                    for(Integer p=0; p<chargebackRates.size(); p++){
                        if(chargebackRates.get(p).SKU__c == currentSKU){
                            skusByLineItems.get(lineItems.get(li).id).add(chargebackRates.get(p));
                        } else if(chargebackRates.get(p).Contractor_Company2__c == lineItems.get(li).CKSW_BASE__Service__r.Contractor_Company2__c){
                            skusByLineItems.get(lineItems.get(li).id).add(chargebackRates.get(p));
                        }
                    }
                }
            }
        }

        RecordType chargebackRecordType = [SELECT id FROM RecordType WHERE SobjectType = 'PayoutChargeback__c' AND DeveloperName='Chargeback'];


        for(CKSW_BASE__Service__c service : chargebackServices){
            PayoutChargeback__c newChargeback = new PayoutChargeback__c(
                    RecordTypeId=chargebackRecordType.id);
            for(CKSW_BASE__Service_Order__c warrantyProject : projectsWithChargebacks){
                if(warrantyProject.Initial_Project__c == service.CKSW_BASE__Service_Order__c){
                    newChargeback.Warranty_Project__c = warrantyProject.id;
                }
            }

            for(CKSW_BASE__Service_Product__c lineItem : lineItems){
                if(lineItem.CKSW_BASE__Service__c==service.id){
                    newChargeback.Work_Order_Line__c = lineItem.id;
                    newChargeback.Quantity__c = lineItem.CKSW_BASE__Quantity_Used__c;
                    newChargeback.Unit_of_Measure__c = lineItem.Service_Units__c;
                    newChargeback.Contractor_Company2__c = lineItem.CKSW_BASE__Service__r.Contractor_Company2__c;
                    newChargeback.Unit_Price__c = lineItem.Service_Unit_Price__c;
                }
            }

            List<Chargeback_Rate__c> possibleChargebackRates = skusByLineItems.get(newChargeback.Work_Order_Line__c);
            System.debug(possibleChargebackRates);
            if(possibleChargebackRates != null){
                for(Integer c=0; c<possibleChargebackRates.size(); c++){
                    if(c==0 || possibleChargebackRates.get(c).Rate_Type__c == 'Contractor Company SKU'){
                        newChargeback.Chargeback_Rate__c = possibleChargebackRates.get(c).id;
                    } else if(c>0 && possibleChargebackRates.get(c).Rate_Type__c == 'Contractor Company'){
                        newChargeback.Chargeback_Rate__c = possibleChargebackRates.get(c).id;
                    }
                }
            }

            System.debug(newChargeback);
            chargebacksToCreate.add(newChargeback);
        }
        insert chargebacksToCreate;

        List<PayoutChargeback__c> chargebacksAdded = [
                SELECT id, Warranty_Project__c, Work_Order_Line__c
                FROM PayoutChargeback__c
                WHERE RecordTypeId=:chargebackRecordType.id AND Work_Order_Line__c IN :lineItems];


        Map<id, List<PayoutChargeback__c>> chargebacksToLineItemId = new Map<id, List<PayoutChargeback__c>>();
        for(CKSW_BASE__Service_Product__c lineItem : lineItems){
            if(!chargebacksToLineItemId.containsKey(lineItem.id)){
                chargebacksToLineItemId.put(lineItem.id, new List<PayoutChargeback__c>());
            }
            for(PayoutChargeback__c chargeback : chargebacksAdded){
                if(chargeback.Work_Order_Line__c == lineItem.id){
                    chargebacksToLineItemId.get(lineItem.id).add(chargeback);
                }
            }
        }

        System.debug(chargebacksToLineItemId);

        List<CKSW_BASE__Service_Order__c> chargebacksCompleteOrInstallmentDateUpdated = new List<CKSW_BASE__Service_Order__c>();

        for(CKSW_BASE__Service_Product__c lineItem : lineItems){
            for(CKSW_BASE__Service__c service : chargebackServices){
                if(lineItem.CKSW_BASE__Service__c == service.id){
                    for(CKSW_BASE__Service_Order__c warrantyProject : projectsWithChargebacks){
                        if(warrantyProject.Initial_Project__c == service.CKSW_BASE__Service_Order__c){
                            if(warrantyProject.Number_of_installments__c == chargebacksToLineItemId.get(lineItem.id).size()){
                                warrantyProject.Chargebacks_Complete__c = TRUE;
                                warrantyProject.Chargeback_Status__c = 'Complete';
                                chargebacksCompleteOrInstallmentDateUpdated.add(warrantyProject);
                            } else if(chargebacksToLineItemId.get(lineItem.id).size() > 0){
                                if(warrantyProject.Interval__c == 'Monthly'){
                                    warrantyProject.Next_Installment_Date__c = warrantyProject.Next_Installment_Date__c.addMonths(1);
                                } else if(warrantyProject.Interval__c == 'Weekly'){
                                    System.debug('HERE');
                                    warrantyProject.Next_Installment_Date__c = warrantyProject.Next_Installment_Date__c.addDays(7);
                                }
                                chargebacksCompleteOrInstallmentDateUpdated.add(warrantyProject);
                            }


                        }
                    }
                }
            }
        }

        System.debug(chargebacksCompleteOrInstallmentDateUpdated);

       update chargebacksCompleteOrInstallmentDateUpdated;
    }

    global static String scheduleDaily(){
        CreateChargebacks newChargebacks = new CreateChargebacks();
        return System.schedule('Chargebacks created', scheddailymidnight, newChargebacks);
    }

}