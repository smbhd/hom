/**
 * Created by AlexSanborn on 4/13/2017.
 */

global with sharing class ChargebackPreviewWrapper {
    @AuraEnabled
    global List<Date> schedule {get;set;}
    @AuraEnabled
    global CKSW_BASE__Service_Product__c lineItem {get;set;}
    @AuraEnabled
    global Chargeback_Rate__c chargebackRate {get;set;}
    @AuraEnabled
    global String itemName{get;set;}
    @AuraEnabled
    global String contractorName{get;set;}
    @AuraEnabled
    global Decimal amtRecurrence {get;set;}
}