/**
 * Created by AsharSaad on 3/20/2017.
 */

global class HomeDepot extends Auth.AuthProviderPluginClass {
	global String getCustomMetadataType() {
		return 'HomeDepotAPI__mdt';
	}

	global String getToken(Map<String, String> authProviderConfiguration) {
		String url = authProviderConfiguration.get('api_url__c');
		String client_id = authProviderConfiguration.get('client_id__c');
		String client_secret = authProviderConfiguration.get('client_secret__c');

		HttpRequest req = new HttpRequest();
		req.setEndpoint(url);
		req.setHeader('Authorization', EncodingUtil.base64Encode(Blob.valueOf(client_id + ':' + client_secret)));
		req.setMethod('GET');

		Http http = new Http();
		HttpResponse res = http.send(req);

		System.debug(req.getHeader('Authorization'));
		System.debug(res.getStatus());
		System.debug(res.getBody());

		Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
		return (String)result.get('access_token');
	}

	global PageReference initiate(Map<String, String> authProviderConfiguration, String stateToPropagate) {
		return new PageReference('https://cs51.salesforce.com/apex/HomeDepotInitiate?state='+stateToPropagate);
	}

	global Auth.AuthProviderTokenResponse handleCallback(Map<String, String> authProviderConfiguration, Auth.AuthProviderCallbackState state) {
		String token = getToken(authProviderConfiguration);

		return new Auth.AuthProviderTokenResponse('HomeDepot', token, 'refreshToken', state.queryParameters.get('state'));
	}

	global override Auth.OAuthRefreshResult refresh(Map<String, String> authProviderConfiguration, String refreshToken) {
		String token = getToken(authProviderConfiguration);

		return new Auth.OAuthRefreshResult(token, 'refreshToken');
	}

	global Auth.UserData getUserInfo(Map<String, String> authProviderConfiguration, Auth.AuthProviderTokenResponse response) {
		return new Auth.UserData(null, null, null, null, null, null, null, null, 'HomeDepot', null, null);
	}
}