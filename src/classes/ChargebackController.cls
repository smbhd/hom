/**
 * Created by AlexSanborn on 4/5/2017.
 */

public with sharing class ChargebackController {
    @AuraEnabled
    public static List<ChargebackWrapper> getReadyToScheduleChargebacks(){
        List<ChargebackWrapper> readytoScheduleChargebacks = new List<ChargebackWrapper>();

        RecordType chargebackRecordType = [SELECT id FROM RecordType WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        List<CKSW_BASE__Service_Order__c> chargebackServiceOrders = [
                SELECT id, Name, Initial_Project__c, Chargeback_Status__c, Number_of_Installments__c, Initial_Work_Order__c
                FROM CKSW_BASE__Service_Order__c
                WHERE RecordTypeId=:chargebackRecordType.id AND
                Chargeback_Status__c !='Schedule Complete' AND
                Chargeback_Status__c != 'Complete' AND
                (Chargeback_Status__c='Ready to Schedule'
                OR CKSW_BASE__Status__c='Closed')];


        List<id> serviceIds = new List<id>();
        for(Integer so=0; so<chargebackServiceOrders.size(); so++){
            serviceIds.add(chargebackServiceOrders.get(so).Initial_Work_Order__c);
        }

        System.debug(chargebackServiceOrders);

        List<CKSW_BASE__Service__c> chargebackServices = [
                SELECT id, Contractor_Company2__r.Name, CKSW_BASE__Service_Type__c, CKSW_BASE__Service_Order__c
                FROM CKSW_BASE__Service__c
                WHERE id IN :serviceIds];


        System.debug(chargebackServices);
        List<CKSW_BASE__Service_Product__c> lineItems = [SELECT id, CKSW_BASE__Quantity_Used__c, Service_Units__c, CKSW_BASE__Service__r.CKSW_BASE__Service_Order__c,
                                                            Service_Unit_Price__c, SKU__c, CKSW_BASE__Service__r.Contractor_Company2__r.Name, Purchase_Order_Line__c
                                                            FROM CKSW_BASE__Service_Product__c WHERE CKSW_BASE__Service__c IN :chargebackServices];
        System.debug(lineItems);

        List<id> poLineItems = new List<id>();

        for(CKSW_BASE__Service_Product__c woLI : lineItems){
            poLineItems.add(woLI.Purchase_Order_Line__c);
        }

        List<Purchase_Order_Line__c> poLines = [SELECT id, Item_Name__c FROM Purchase_Order_Line__c WHERE id IN :poLineItems];
        System.debug(poLines);

        List<id> skusFromLineItems = new List<id>();
        if(lineItems != null){
            for(CKSW_BASE__Service_Product__c lineItem :lineItems){
                if(lineItem.SKU__c != null){
                    skusFromLineItems.add(lineItem.SKU__c);
                }
            }
        }

        List<id> contractorCompanyIds = new List<id>();
        for(Integer cs=0; cs<chargebackServices.size(); cs++){
            contractorCompanyIds.add(chargebackServices.get(cs).Contractor_Company2__c);
        }

        List<Chargeback_Rate__c> chargebackRates = [SELECT id, Name, Contractor_Company2__c, Rate__c, Rate_Type__c, SKU__c, Unit_of_Measure__c
                                                    FROM Chargeback_Rate__c WHERE (SKU__c IN :skusFromLineItems) OR (Contractor_Company2__c IN :contractorCompanyIds)];

        System.debug(chargebackRates);

        Map<id, List<Chargeback_Rate__c>> skusByLineItems = new Map <id, List<Chargeback_Rate__c>>();

        if(lineItems != null){
            for(CKSW_BASE__Service_Product__c lineItem : lineItems){
                if((lineItem.SKU__c != null && !skusByLineItems.containsKey(lineItem.id))){
                    List<Chargeback_Rate__c> chargebackRatesList = new List<Chargeback_Rate__c>();
                    skusByLineItems.put(lineItem.id, chargebackRatesList);
                }

                if(skusByLineItems.containsKey(lineItem.id)){
                    Id currentSKU = lineItem.SKU__c;
                    for(Integer p=0; p<chargebackRates.size(); p++){
                        if(chargebackRates.get(p).SKU__c == currentSKU){
                            skusByLineItems.get(lineItem.id).add(chargebackRates.get(p));
                        } else if(chargebackRates.get(p).Contractor_Company2__c == lineItem.CKSW_BASE__Service__r.Contractor_Company2__c){
                            skusByLineItems.get(lineItem.id).add(chargebackRates.get(p));
                        }
                    }
                }
            }
        }

        System.debug(skusByLineItems);

        //mapping service orders, chargeback rates, and line items into wrapper
        for(Integer so=0; so<chargebackServiceOrders.size(); so++){
            ChargebackWrapper newChargeback = new ChargebackWrapper();
            newChargeback.project = chargebackServiceOrders.get(so);
            newChargeback.projectName = chargebackServiceOrders.get(so).Name;

            for(CKSW_BASE__Service_Product__c lineItem :lineItems){
                System.debug(lineItem.CKSW_BASE__Service__r.CKSW_BASE__Service_Order__c);
                System.debug(newChargeback.project.Initial_Project__c);
                if(lineItem.CKSW_BASE__Service__r.CKSW_BASE__Service_Order__c == newChargeback.project.Initial_Project__c){

                    newChargeback.lineItem = new CKSW_BASE__Service_Product__c();
                    newChargeback.lineItem = lineItem;
                    newChargeback.contractorName = lineItem.CKSW_BASE__Service__r.Contractor_Company2__r.Name;
                    for(Purchase_Order_Line__c po : poLines){
                        System.debug(po.id);
                        System.debug(lineItem.Purchase_Order_Line__c);
                        if(po.id == lineItem.Purchase_Order_Line__c){
                            newChargeback.itemName = po.Item_Name__c;
                        }
                    }

                    List<Chargeback_Rate__c> possibleChargebackRates = skusByLineItems.get(newChargeback.lineItem.id);
                    if(possibleChargebackRates != null){
                        newChargeback.chargebackRate = new Chargeback_Rate__c();
                        for(Integer c=0; c<possibleChargebackRates.size(); c++){
                            if(c==0 || possibleChargebackRates.get(c).Rate_Type__c == 'Contractor Company SKU'){
                                newChargeback.chargebackRate = possibleChargebackRates.get(c);
                            } else if(c>0 && possibleChargebackRates.get(c).Rate_Type__c == 'Contractor Company'){
                                newChargeback.chargebackRate = possibleChargebackRates.get(c);
                            }
                        }
                    }
                    newChargeback.hasLineItems = TRUE;
                }
            }
            //if has no line items...set boolean to flag when rendering
            if(newChargeback.lineItem == null){
                newChargeback.hasLineItems = FALSE;
            }

            readytoScheduleChargebacks.add(newChargeback);
        }
        System.debug(readytoScheduleChargebacks);
        return readytoScheduleChargebacks;
    }

    @AuraEnabled
    public static ChargebackPreviewWrapper scheduleChargeback(String chargebackJSON, String numInstallments, String installationDate, String intervals){
        System.debug(chargebackJSON);
        System.debug(numInstallments);
        System.debug(installationDate);
        System.debug(intervals);

        ChargebackWrapper schedulableChargeback = (ChargebackWrapper) JSON.deserialize(chargebackJSON, ChargebackWrapper.class);

        CKSW_BASE__Service_Order__c projectToSchedule = new CKSW_BASE__Service_Order__c(id=schedulableChargeback.project.id);

        projectToSchedule.Number_of_Installments__c = Integer.valueOf(numInstallments);
        projectToSchedule.First_Installment_Date__c = date.valueOf(installationDate);
        projectToSchedule.Next_Installment_Date__c = projectToSchedule.First_Installment_Date__c;
        projectToSchedule.Interval__c = intervals;
        projectToSchedule.Chargeback_Status__c = 'Schedule Complete';
        update projectToSchedule;


        ChargebackPreviewWrapper chargebackPreview = new ChargebackPreviewWrapper();
        chargebackPreview.lineItem = schedulableChargeback.lineItem;
        chargebackPreview.itemName = schedulableChargeback.itemName; //
        chargebackPreview.contractorName = schedulableChargeback.contractorName; //
        chargebackPreview.chargebackRate= schedulableChargeback.chargebackRate;
        chargebackPreview.amtRecurrence = (chargebackPreview.lineItem.Service_Unit_Price__c*chargebackPreview.lineItem.CKSW_BASE__Quantity_Used__c)/projectToSchedule.Number_of_Installments__c;

        chargebackPreview.schedule = previewChargebackSchedule(schedulableChargeback, numInstallments, installationDate, intervals); //
        System.debug(chargebackPreview);
        return chargebackPreview;
    }

    public static List<Date> previewChargebackSchedule(ChargebackWrapper chargeback, String numInstallments, String installationDate, String intervals){
        List<Date> chargebackSchedule = new List<Date>();
        Date nextInstallationDate = date.valueOf(installationdate);
        Integer numInstallmentsInt = Integer.valueOf(numInstallments);

        while(numInstallmentsInt > 0){
            chargebackSchedule.add(nextInstallationDate);
            if(intervals == 'Monthly'){
                nextInstallationDate = nextInstallationDate.addMonths(1);
            } else{
                nextInstallationDate = nextInstallationDate.addDays(7);
            }
            numInstallmentsInt--;
        }
        return chargebackSchedule;
    }
}