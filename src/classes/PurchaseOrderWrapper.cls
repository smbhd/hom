/**
 * Created by AlexSanborn on 3/23/2017.
 */

    global class PurchaseOrderWrapper{
            @AuraEnabled
            global Integer index;
            @AuraEnabled
            global Purchase_Order__c purchaseOrder;
            @AuraEnabled
            global List<LineItemWrapper> purchaseOrderLines;
            @AuraEnabled
            global Account account;
            @AuraEnabled
            global Contact contact;
            @AuraEnabled
            global List<String> errorMessages;
            @AuraEnabled
            global Boolean hasError;
            @AuraEnabled
            global Boolean hasProjectId;
            @AuraEnabled
            global String projectId;
            @AuraEnabled
            global Account branch;
            @AuraEnabled
            global Contact branchContact;
            @AuraEnabled
            global Boolean verifiedAddress;
            @AuraEnabled
            global Boolean toDelete;

            @AuraEnabled
            global String billingStreet;
            @AuraEnabled
            global String billingCity;
            @AuraEnabled
            global String billingState;
            @AuraEnabled
            global String billingPostalCode;
            @AuraEnabled
            global String phone;
            @AuraEnabled
            global String altPhone;

    global class LineItemWrapper {
        @AuraEnabled
        global Purchase_Order_Line__c purchaseOrderLine;
        @AuraEnabled
        global String lineItemErrorMessage;
        @AuraEnabled
        global Boolean lineItemHasError;
        @AuraEnabled
        global Decimal suggestedPrice;
        @AuraEnabled
        global Decimal quantity;
        @AuraEnabled
        global Decimal unitPrice;
    }
}

