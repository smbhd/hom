/**
 * Created by AsharSaad on 3/21/2017.
 */

public class CalloutResponse {
	public String body;
	public Integer statuscode;

	public CalloutResponse(String body, Integer statuscode) {
		this.body = body;
		this.statuscode = statuscode;
	}
}