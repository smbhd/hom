/**
 * Created by AlexSanborn on 4/12/2017.
 */

@isTest
public class CreateChargebacksTest {
    @isTest
    public static void schedulerFiresDaily(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Schedule Complete',
                Interval__c='Weekly',
                Number_of_Installments__c=3,
                Next_Installment_Date__c=Date.Today()
        );
        insert newChargebackServiceOrder;

        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);

        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;

        Test.startTest();
        String jobId = CreateChargebacks.scheduleDaily();
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id=:jobId];
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
    //improves test coverage by 4%

    @isTest
    public static void scheduleCreatesNewChargebacksAndUpdatesNextWeeklyInstallmentDate(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Schedule Complete',
                Interval__c='Weekly',
                Number_of_Installments__c=3,
                Next_Installment_Date__c=Date.Today()
        );
        insert newChargebackServiceOrder;



        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);

        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;
        Integer chargebacksInitial = [SELECT Count() FROM PayoutChargeback__c];
        System.assertEquals(0, chargebacksInitial);

        Test.startTest();
        System.schedule('Chargebacks created', '0 0 0 3 9 ? 2022', new CreateChargebacks());
        Test.stopTest();

        RecordType warrantyRecordType = [SELECT id FROM RecordType WHERE SobjectType='CKSW_Base__Service_Order__c' AND DeveloperName='Warranty_Project'];

        PayoutChargeback__c chargebackCreated = [SELECT id, Warranty_Project__c FROM PayoutChargeback__c LIMIT 1];
        CKSW_BASE__Service_Order__c projectWeekly = [SELECT id, Next_Installment_Date__c FROM CKSW_BASE__Service_Order__c WHERE RecordTypeId = :warrantyRecordType.id];


        System.assertEquals(chargebackCreated.Warranty_Project__c, projectWeekly.id);
        System.assertEquals(Date.today().addDays(7), projectWeekly.Next_Installment_Date__c);
    }

    @isTest
    public static void scheduleCreatesNewChargebacksAndUpdatesNextMonthlyInstallmentDate(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Schedule Complete',
                Interval__c='Monthly',
                Number_of_Installments__c=3,
                Next_Installment_Date__c=Date.Today()
        );
        insert newChargebackServiceOrder;



        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);

        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;

        Integer chargebacksInitial = [SELECT Count() FROM PayoutChargeback__c];
        System.assertEquals(0, chargebacksInitial);

        CKSW_BASE__Service_Order__c projectMonth = [SELECT id, Interval__c FROM CKSW_BASE__Service_Order__c LIMIT 1];
        projectMonth.Interval__c = 'Monthly';
        update projectMonth;

        Test.startTest();
        System.schedule('Chargebacks created', '0 0 0 3 9 ? 2022', new CreateChargebacks());
        Test.stopTest();
        RecordType warrantyRecordType = [SELECT id FROM RecordType WHERE SobjectType='CKSW_Base__Service_Order__c' AND DeveloperName='Warranty_Project'];

        PayoutChargeback__c chargebackCreated = [SELECT id, Warranty_Project__c FROM PayoutChargeback__c LIMIT 1];
        CKSW_BASE__Service_Order__c projectMonthly = [SELECT id, Next_Installment_Date__c FROM CKSW_BASE__Service_Order__c WHERE RecordTypeId = :warrantyRecordType.id LIMIT 1];

        System.assertEquals(chargebackCreated.Warranty_Project__c, projectMonthly.id);
        System.assertEquals(Date.today().addMonths(1), projectMonthly.Next_Installment_Date__c);
    }

    @isTest
    public static void scheduleCreatesNewChargebacksAndMarksCompletedChargebackProjects(){
        List<CKSW_BASE__Service_Type__c> skus = new List<CKSW_BASE__Service_Type__c>();
        List<SKU_Price__c> prices = new List<SKU_Price__c>();
        List<CKSW_BASE__Service_Type_Required_Product__c> skuProducts = new List<CKSW_BASE__Service_Type_Required_Product__c>();
        List<Chargeback_Rate__c> chargebackRates = new List<Chargeback_Rate__c>();

        Account newAccount = new Account(Name='test acct');
        insert newAccount;

        CKSW_BASE__Service_Order__c newProject = new CKSW_BASE__Service_Order__c(CKSW_BASE__Status__c='Opened', CKSW_BASE__Account__c=newAccount.id);
        insert newProject;

        RecordType chargebackRecordType = [SELECT id FROM RecordType
        WHERE SobjectType='CKSW_BASE__Service_Order__c' AND DeveloperName='Warranty_Project'];

        RecordType ContractorRecordType = [SELECT id FROM RecordType WHERE SobjectType='Account' AND DeveloperName='Contractor_Company'];
        Account contractorCompany = new Account(Name='The Contractors', RecordTypeId=ContractorRecordType.id);
        insert contractorCompany;

        CKSW_BASE__Service_Type__c sku1 = new CKSW_BASE__Service_Type__c(Name='flooring', CKSW_BASE__Duration__c=3.00, CKSW_BASE__Duration_Type__c='Hours');
        CKSW_BASE__Service_Type__c sku2 = new CKSW_BASE__Service_Type__c(Name='paint', CKSW_BASE__Duration__c=4.00, CKSW_BASE__Duration_Type__c='Hours');
        skus.add(sku1);
        skus.add(sku2);
        insert skus;

        CKSW_BASE__Service_Type_Required_Product__c skuProduct1 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku1.id, CKSW_BASE__Quantity_Required__c=4);
        CKSW_BASE__Service_Type_Required_Product__c skuProduct2 = new CKSW_BASE__Service_Type_Required_Product__c(CKSW_BASE__Service_Type__c=sku2.id, CKSW_BASE__Quantity_Required__c=4);
        skuProducts.add(skuProduct1);
        skuProducts.add(skuProduct2);
        insert skuProducts;


        SKU_Price__c skuprice1 = new SKU_Price__c(Name='flooring global', isGlobal__c=TRUE, SKU__c=skuProduct1.id, Unit_Price__c=4.5, Service_Type__c=sku1.id);
        SKU_Price__c skuprice2 = new SKU_Price__c(Name='paint global', isGlobal__c=TRUE, SKU__c=skuProduct2.id, Unit_Price__c=1.2, Service_Type__c=sku2.id);
        prices.add(skuprice1);
        prices.add(skuprice2);
        insert prices;

        CKSW_BASE__Service__c newService1 = new CKSW_BASE__Service__c();
        newService1.CKSW_BASE__Service_Order__c = newProject.id;
        newService1.CKSW_BASE__Status__c = 'New';
        newService1.Contractor_Company2__c = contractorCompany.id;
        newService1.CKSW_BASE__Service_Type__c = sku1.id;
        insert newService1;

        CKSW_BASE__Service_Order__c newChargebackServiceOrder = new CKSW_BASE__Service_Order__c(
                RecordTypeId=chargebackRecordType.id,
                Initial_Work_Order__c = newService1.id,
                CKSW_BASE__Status__c='Close',
                Chargeback_Status__c='Schedule Complete',
                Interval__c='Weekly',
                Number_of_Installments__c=1,
                Next_Installment_Date__c=Date.Today()
        );
        insert newChargebackServiceOrder;

        Purchase_Order__c newPOChargeback = new Purchase_Order__c(Project__c=newChargebackServiceOrder.id);
        insert newPOChargeback;

        Purchase_Order_Line__c newLI1 = new Purchase_Order_Line__c(Item_Name__c='flooring', Quantity__c=7, Unit_Price__c=4.5, Purchase_Order__c=newPOChargeback.id, SKU__c=sku1.id);Purchase_Order_Line__c newLI2 = new Purchase_Order_Line__c(Item_Name__c='paint', Quantity__c=4, Unit_Price__c=1.2, Purchase_Order__c=newPOChargeback.id, SKU__c=sku2.id);
        insert newLI1;

        CKSW_BASE__Service_Product__c newWOLI1 = new CKSW_BASE__Service_Product__c(
                CKSW_BASE__Quantity_Used__c=5,
                Service_Units__c='Square Foot',
                CKSW_BASE__Service__c=newService1.id,
                SKU__c=sku1.id,
                Purchase_Order_Line__c=newLI1.id,
                Service_Unit_Price__c=4.00);

        insert newWOLI1;

        Chargeback_Rate__c newChargebackRate1 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, SKU__c=sku1.id, Rate__c=4.4);
        Chargeback_Rate__c newChargebackRate2 = new Chargeback_Rate__c(Contractor_Company2__c=contractorCompany.id, Rate__c=5.6);
        Chargeback_Rate__c newChargebackRate3 = new Chargeback_Rate__c(isGlobal__c=TRUE, SKU__c=sku1.id, Rate__c=4.5);
        chargebackRates.add(newChargebackRate3);
        chargebackRates.add(newChargebackRate2);
        chargebackRates.add(newChargebackRate1);
        insert chargebackRates;

        Integer chargebacksInitial = [SELECT Count() FROM PayoutChargeback__c];
        System.assertEquals(0, chargebacksInitial);

        CKSW_BASE__Service_Order__c project = [SELECT id, Number_of_Installments__c FROM CKSW_BASE__Service_Order__c LIMIT 1];
        project.Number_of_Installments__c = 1;
        update project;

        Test.startTest();
        System.schedule('Chargebacks created', '0 0 0 3 9 ? 2022', new CreateChargebacks());
        Test.stopTest();
        RecordType warrantyRecordType = [SELECT id FROM RecordType WHERE SobjectType='CKSW_Base__Service_Order__c' AND DeveloperName='Warranty_Project'];

        PayoutChargeback__c chargebackCreated = [SELECT id, Warranty_Project__c FROM PayoutChargeback__c LIMIT 1];
        CKSW_BASE__Service_Order__c projectCompleted = [
                SELECT id, Next_Installment_Date__c, Chargebacks_Complete__c
                FROM CKSW_BASE__Service_Order__c
                WHERE RecordTypeId = :warrantyRecordType.id LIMIT 1];
        System.assertEquals(chargebackCreated.Warranty_Project__c, projectCompleted.id);
        System.assertEquals(Date.today(), projectCompleted.Next_Installment_Date__c);
        System.assertEquals(TRUE, projectCompleted.Chargebacks_Complete__c);
    }
}