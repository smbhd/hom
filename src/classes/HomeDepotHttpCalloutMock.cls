/**
 * Created by AlexSanborn on 5/10/2017.
 */

global class HomeDepotHttpCalloutMock implements HttpCalloutMock{
    global HttpResponse respond(HttpRequest request){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"success"}'); //dummy response for now
        res.setStatusCode(200);
        return res;
    }
}