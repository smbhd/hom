/**
 * Created by AlexSanborn on 5/9/2017.
 */

@IsTest(SeeAllData=true)
public class AddPurchaseOrderControllerTest {
    @IsTest
    public static void getsPurchaseOrdersRelatedToAccount(){
        Account newAccount = new Account(Name='4147 Ravenswood');
        insert newAccount;

        List<CKSW_BASE__Service_Order__c> newProjects = new List<CKSW_BASE__Service_Order__c>();
        CKSW_BASE__Service_Order__c project1 = new CKSW_BASE__Service_Order__c(CKSW_BASE__Account__c=newAccount.id);
        CKSW_BASE__Service_Order__c project2 = new CKSW_BASE__Service_Order__c(CKSW_BASE__Account__c=newAccount.id);
        CKSW_BASE__Service_Order__c project3 = new CKSW_BASE__Service_Order__c(CKSW_BASE__Account__c=newAccount.id);
        CKSW_BASE__Service_Order__c project4 = new CKSW_BASE__Service_Order__c(CKSW_BASE__Account__c=newAccount.id);
        newProjects.add(project1);
        newProjects.add(project2);
        newProjects.add(project3);
        newProjects.add(project4);
        insert newProjects;

        List<Purchase_Order__c> newPOs = new List<Purchase_Order__c>();
        Purchase_Order__c newPO1 = new Purchase_Order__c(Project__c=project1.id, Order_Status__c='Open');
        Purchase_Order__c newPO2 = new Purchase_Order__c(Project__c=project2.id, Order_Status__c='Open');
        Purchase_Order__c newPO3 = new Purchase_Order__c(Project__c=project3.id, Order_Status__c='Open');
        Purchase_Order__c newPO4 = new Purchase_Order__c(Project__c=project4.id, Order_Status__c='Open');
        newPOs.add(newPO1);
        newPOs.add(newPO2);
        newPOs.add(newPO3);
        newPOs.add(newPO4);
        insert newPOs;

        List<Purchase_Order_Line__c> newLines = new List<Purchase_Order_Line__c>();
        Purchase_Order_Line__c newLine1 = new Purchase_Order_Line__c(Purchase_Order__c = newPO1.id);
        Purchase_Order_Line__c newLine2 = new Purchase_Order_Line__c(Purchase_Order__c = newPO2.id);
        Purchase_Order_Line__c newLine3 = new Purchase_Order_Line__c(Purchase_Order__c = newPO3.id);
        Purchase_Order_Line__c newLine4 = new Purchase_Order_Line__c(Purchase_Order__c = newPO4.id);
        newLines.add(newLine1);
        newLines.add(newLine2);
        newLines.add(newLine3);
        newLines.add(newLine4);
        insert newLines;

        List<AddPurchaseOrderController.poWithLineItems> purchaseOrders = AddPurchaseOrderController.getAllOpenPurchaseOrdersAndLineItems(project1.id);

        System.assertEquals(3, purchaseOrders.size());
    }

    @IsTest
    public static void updatesPurchaseOrderWithNewProject(){
        List<CKSW_BASE__Service_Order__c> newProjects = new List<CKSW_BASE__Service_Order__c>();
        CKSW_BASE__Service_Order__c project1 = new CKSW_BASE__Service_Order__c();
        CKSW_BASE__Service_Order__c project2 = new CKSW_BASE__Service_Order__c();
        newProjects.add(project1);
        newProjects.add(project2);
        insert newProjects;

        Purchase_Order__c newPO1 = new Purchase_Order__c(Project__c=project1.id, Order_Status__c='Open');
        insert newPO1;

        System.assertEquals(newPO1.Project__c, project1.id);
        AddPurchaseOrderController.addPurchaseOrder(project2.id, newPO1.id);
        Purchase_Order__c updatedPO = [SELECT id, Project__c FROM Purchase_Order__c WHERE id=:newPO1.id];
        System.assertEquals(updatedPO.Project__c, project2.id);
    }

}