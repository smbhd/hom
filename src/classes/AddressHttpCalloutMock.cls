/**
 * Created by HabibKhan on 5/10/2017.
 */

global class AddressHttpCalloutMock implements HttpCalloutMock{

    global HttpResponse respond(HttpRequest request){

        //valid response from server
        String body ='[{"place_id":"58252694","osm_type":"way","osm_id":"5704001","boundingbox":["40.7577582","40.7620095","-73.7811854","-73.7791438"],"lat":"40.760472","lon":"-73.7804189","display_name":"204th Street, Bayside, Queens, Queens County, NYC, New York, 11361, United States of America","class":"highway","type":"residential","importance":0.61,"address":{"road":"204th Street","neighbourhood":"Bayside","suburb":"Queens","county":"Queens County","city":"NYC","state":"New York","postcode":"11361","country":"United States of America","country_code":"us"}},{"place_id":"57063167","osm_type":"way","osm_id":"5703997","boundingbox":["40.75364","40.7577022","-73.7794031","-73.7763709"],"lat":"40.755622","lon":"-73.7782679","display_name":"204th Street, Oakland Gardens, Queens, Queens County, NYC, New York, 11361, United States of America","class":"highway","type":"residential","importance":0.51,"address":{"road":"204th Street","neighbourhood":"Oakland Gardens","suburb":"Queens","county":"Queens County","city":"NYC","state":"New York","postcode":"11361","country":"United States of America","country_code":"us"}},{"place_id":"57616339","osm_type":"way","osm_id":"5703995","boundingbox":["40.775075","40.777194","-73.7891839","-73.7882699"],"lat":"40.775075","lon":"-73.7882699","display_name":"204th Street, Bayside, Queens, Queens County, NYC, New York, 11360, United States of America","class":"highway","type":"residential","importance":0.5,"address":{"road":"204th Street","neighbourhood":"Bayside","suburb":"Queens","county":"Queens County","city":"NYC","state":"New York","postcode":"11360","country":"United States of America","country_code":"us"}}]';

        HttpResponse res = new HttpResponse();
        res.setBody(body);

        return res;
    }

}