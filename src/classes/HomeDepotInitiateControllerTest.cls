/**
 * Created by AlexSanborn on 5/11/2017.
 */

@isTest
public with sharing class HomeDepotInitiateControllerTest {

    @isTest
    public static void testCallout(){
        HomeDepotInitiateController homeDepot = new HomeDepotInitiateController();
        PageReference actualCallout = homeDepot.callout();
        PageReference expectedCallout = new PageReference('https://cs51.salesforce.com/services/authcallback/00D4B000000CyEaUAK/HomeDepot?state=null');

        System.assertEquals(actualCallout.getUrl(), expectedCallout.getUrl());
    }
}