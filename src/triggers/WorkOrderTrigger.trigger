/**
 * Created by AlexSanborn on 4/25/2017.
 */

trigger WorkOrderTrigger on CKSW_BASE__Service__c (after update) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
           WorkOrderService.createPayoutWhenStatusClosed(Trigger.new);
        }
    }

}