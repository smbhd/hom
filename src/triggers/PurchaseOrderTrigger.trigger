/**
 * Created by AlexSanborn on 5/3/2017.
 */

trigger PurchaseOrderTrigger on Purchase_Order__c (after update) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            PurchaseOrderService.deleteProjectsWithoutPO(Trigger.old);
        }
    }

}